﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using mt.libraries.domain.Peripherals.Serial;
using MT.Singularity.Composition;
using MT.Singularity.Platform.Devices;
using MT.Singularity.Platform.Infrastructure;

namespace mt.libraries.interfaces.Serial
{
    [Export(typeof(ISerialInterfaceProviderService))]
    [InjectionBehavior(IsSingleton = true)]
    public class SerialInterfaceProviderService : ISerialInterfaceProviderService
    {

        public async Task<ISerialInterface> FindSerialInterfaceByPortname(IPlatformEngine engine, string portname)
        {
            var interfaceservice = await engine.GetInterfaceServiceAsync();
            var allinterfaces = await interfaceservice.GetAllInterfacesAsync();
            var allserialinterfaces = allinterfaces.OfType<ISerialInterface>().ToList();
            var serialInterface = allserialinterfaces.FirstOrDefault(serial => serial.PortName == portname);


            return serialInterface;

        }

        public async Task<ISerialInterface> FindSerialInterfacebyPhysicalPort(IPlatformEngine engine, int physicalPort)
        {
            var interfaceservice = await engine.GetInterfaceServiceAsync();
            var allinterfaces = await interfaceservice.GetAllInterfacesAsync();
            var allserialinterfaces = allinterfaces.OfType<ISerialInterface>().ToList();
            var serialInterface = allserialinterfaces.FirstOrDefault(serial => serial.LogicalPort == physicalPort);


            return serialInterface;

        }
    }
}
