﻿using System;
using System.Threading.Tasks;
using log4net;
using mt.libraries.domain.Peripherals.DigitalIo;
using MT.Singularity.Composition;
using MT.Singularity.Logging;
using MT.Singularity.Platform.Devices;
using MT.Singularity.Platform.Infrastructure;

namespace mt.libraries.interfaces.DigitalIo
{
    [Export(typeof(IDigitalIo))]
    [InjectionBehavior(IsSingleton = true)]
    public class DigitalIo : IDigitalIo
    {
        private static readonly ILog Logger = Log4NetManager.ApplicationLogger;
        private static readonly string SourceClass = nameof(DigitalIo);
        public event EventHandler<IDigitalInputChangedEventArgs> DigitalInputChangedEvent;
        /// <summary>
        /// The digital 4IO
        /// </summary>
        private IDigitalIOInterface _digitalIo;

        /// <summary>
        /// The interfaces
        /// </summary>
        private IInterfaceService _interfaces;


        public DigitalIo(IPlatformEngine engine)
        {
            RegisterIoInputEvents(engine).ContinueWith(HandleException);
        }

        private void HandleException(Task obj)
        {
            if (obj.Exception != null)
            {
                Logger.ErrorEx("IO Registration Failed: ",SourceClass,obj.Exception);
            }
        }

        private async Task RegisterIoInputEvents(IPlatformEngine engine)
        {
            var platformEngine = engine;
            _interfaces = await platformEngine.GetInterfaceServiceAsync();

            for (int i = 1; i < 7; i++)
            {
                this._digitalIo = await _interfaces.GetDigitalIOInterfaceAsync(i);
                if (this._digitalIo != null)
                {
                    this._digitalIo.InputChanged += this.DigitalIoInputChanged;
                    break;
                }
            }

        }
        /// <summary>
        /// Writes the output by index to digital 4IO.
        /// </summary>
        // ReSharper disable once InconsistentNaming
        private async void WriteOutputByIndexDigital4IO()
        {
            await this._digitalIo.WriteOutputByIndexAsync(2, false);
            await Task.Delay(1000);
            await this._digitalIo.WriteOutputByIndexAsync(2, true);
            await Task.Delay(1000);
            await this._digitalIo.WriteOutputByIndexAsync(2, false);
            await Task.Delay(1000);
            await this._digitalIo.WriteOutputByIndexAsync(2, true);
        }

        /// <summary>
        /// Writes the outputs of the digital4.
        /// </summary>
        // ReSharper disable once InconsistentNaming
        private async void WriteOutputDigital4IO()
        {
            var array = new[] { Output1, Output2, Output3, Output4 };

            if (array.Length > this._digitalIo.NumberOfOutputs)
            {
                StatusInfo = "Error: Number of inputs does not match";
                return;
            }

            if (this._digitalIo != null)
            {
                await this._digitalIo.WriteOutputAsync(array);
                StatusInfo = "Outputs: " + this._digitalIo.Outputs;
            }
        }

        /// <summary>
        /// Reads the inputs of digital4 io.
        /// </summary>
        // ReSharper disable once InconsistentNaming
        private void ReadInputDigital4IO()
        {
            StatusInfo = "inputs: " + _digitalIo.Inputs[0] + ":" + _digitalIo.Inputs[1] + ":" + _digitalIo.Inputs[2] + ":" + _digitalIo.Inputs[3] + ":";
            InputRead1 = _digitalIo.Inputs[0];
            InputRead2 = _digitalIo.Inputs[1];
            InputRead3 = _digitalIo.Inputs[2];
            InputRead4 = _digitalIo.Inputs[3];
        }

        #region InputEvent
        /// <summary>
        /// Digitals the i o_ input changed.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <param name="newValue">if set to <c>true</c> [new value].</param>
        private void DigitalIoInputChanged(int index, bool newValue)
        {
            DigitalInputChangedEvent?.Invoke(this, new DigitalInputChangedEventArgs(index, newValue));
            
        }

        #endregion InputEvent

        #region Outputs
        /// <summary>
        /// Gets or sets the value of MyProperty
        /// </summary>
        public bool Output1
        {
            get { return _output1; }
            set
            {
                if (_output1 != value)
                {
                    _output1 = value;

                }
            }
        }

        private bool _output1;

        /// <summary>
        /// Gets or sets the value of MyProperty
        /// </summary>
        public bool Output2
        {
            get { return _output2; }
            set
            {
                if (_output2 != value)
                {
                    _output2 = value;

                }
            }
        }

        private bool _output2;

        /// <summary>
        /// Gets or sets the value of MyProperty
        /// </summary>
        public bool Output3
        {
            get { return _output3; }
            set
            {
                if (_output3 != value)
                {
                    _output3 = value;

                }
            }
        }

        private bool _output3;

        /// <summary>
        /// Gets or sets the value of MyProperty
        /// </summary>
        public bool Output4
        {
            get { return _output4; }
            set
            {
                if (_output4 != value)
                {
                    _output4 = value;

                }
            }
        }

        private bool _output4;

        #endregion Outputs

     

        #region ReadInputs
        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="HomeScreenViewModel"/> is input1.
        /// </summary>
        public bool InputRead1
        {
            get { return _inputRead1; }
            set
            {
                if (_inputRead1 != value)
                {
                    _inputRead1 = value;

                }
            }
        }

        private bool _inputRead1;

        /// <summary>
        /// Gets or sets the value of MyProperty
        /// </summary>
        public bool InputRead2
        {
            get { return _inputRead2; }
            set
            {
                if (_inputRead2 != value)
                {
                    _inputRead2 = value;

                }
            }
        }

        private bool _inputRead2;

        /// <summary>
        /// Gets or sets the value of MyProperty
        /// </summary>
        public bool InputRead3
        {
            get { return _inputRead3; }
            set
            {
                if (_inputRead3 != value)
                {
                    _inputRead3 = value;

                }
            }
        }

        private bool _inputRead3;

        /// <summary>
        /// Gets or sets the value of MyProperty
        /// </summary>
        public bool InputRead4
        {
            get { return _inputRead4; }
            set
            {
                if (_inputRead4 != value)
                {
                    _inputRead4 = value;

                }
            }
        }

        private bool _inputRead4;

        #endregion ReadInputs

        #region InfoText

        /// <summary>
        /// Gets or sets the value of MyProperty
        /// </summary>
        public string StatusInfo
        {
            get { return _statusInfo; }
            set
            {
                if (_statusInfo != value)
                {
                    _statusInfo = value;

                }
            }
        }

        private string _statusInfo;

        /// <summary>
        /// Gets or sets the value of MyProperty
        /// </summary>
        public string Info
        {
            get { return _info; }
            set
            {
                if (_info != value)
                {
                    _info = value;
                }
            }
        }

        private string _info;

        #endregion InfoText
    }
}

