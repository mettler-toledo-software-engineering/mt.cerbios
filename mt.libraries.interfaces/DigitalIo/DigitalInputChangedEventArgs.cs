﻿using System;
using mt.libraries.domain.Peripherals.DigitalIo;

namespace mt.libraries.interfaces.DigitalIo
{
    public class DigitalInputChangedEventArgs : EventArgs, IDigitalInputChangedEventArgs
    {
        public int Input { get;  }
        public bool NewValue { get; }

        public DigitalInputChangedEventArgs(int input, bool newValue)
        {
            Input = input;
            NewValue = newValue;
        }
    }
}
