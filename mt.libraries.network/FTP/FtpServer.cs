﻿using mt.libraries.domain.Network.FTP;
using MT.Singularity.Composition;

namespace mt.libraries.network.FTP
{
    [Export(typeof(IFtpServer))]
    [InjectionBehavior(IsSingleton = true)]
    public class FtpServer : IFtpServer
    {
        public string FtpServerAndFolder { get; set; }

        public FtpServer(string ftpServerAndFolder)
        {
            FtpServerAndFolder = ftpServerAndFolder;
        }
    }
}
