﻿using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using log4net;
using mt.libraries.domain.Authentication;
using mt.libraries.domain.Network.FTP;
using MT.Singularity.Composition;
using MT.Singularity.Logging;

namespace mt.libraries.network.FTP
{
    [Export(typeof(IFtpFileTransferService))]
    [InjectionBehavior(IsSingleton = true)]
    public class FtpFileTransferService : IFtpFileTransferService
    {
        private readonly IFtpServer _ftpServer;
        private readonly IUser _ftpUser;
        private static readonly ILog Logger = Log4NetManager.ApplicationLogger;
        private static readonly string SourceClass = nameof(FtpFileTransferService);
        public FtpFileTransferService(IFtpServer ftpServer, IUser ftpUser)
        {
            _ftpServer = ftpServer;
            _ftpUser = ftpUser;
        }

        public bool Download(string filename, string targetDirectory)
        {
            try
            {
                using (var client = new WebClient())
                {
                    client.Credentials = new NetworkCredential(_ftpUser.Username, _ftpUser.Password);
                    client.DownloadFile(new Uri(Path.Combine($"ftp://{_ftpServer.FtpServerAndFolder}", filename)), Path.Combine(targetDirectory, filename));

                }

                return true;
            }
            catch (Exception e)
            {
                Logger.ErrorEx("ftp download failed :", SourceClass, e);
                return false;
            }
        }

        public bool Updload(string filename, string sourceDirectory)
        {
            var client = new WebClient();
            try
            {

                client.Credentials = new NetworkCredential(_ftpUser.Username, _ftpUser.Password);
                client.UploadFile(new Uri(Path.Combine(_ftpServer.FtpServerAndFolder, filename)), WebRequestMethods.Ftp.UploadFile, Path.Combine(sourceDirectory, filename));
                client.Dispose();

                return true;
            }
            catch (Exception e)
            {
                client.Dispose();
                Logger.ErrorEx("ftp upload failed :", SourceClass, e);
                return false;
            }

        }

        public async Task<bool> UpdloadAsync(string filename, string sourceDirectory)
        {
            Console.WriteLine(_ftpUser.Username);
            if (FileAndPathExists(sourceDirectory, filename))
            {
                try
                {
                    using (var client = new WebClient())
                    {
                        client.Credentials = new NetworkCredential(_ftpUser.Username, _ftpUser.Password);
                        await client.UploadFileTaskAsync(new Uri(Path.Combine(_ftpServer.FtpServerAndFolder, filename)), WebRequestMethods.Ftp.UploadFile, Path.Combine(sourceDirectory, filename));

                    }

                    return true;
                }
                catch (Exception e)
                {
                    Logger.ErrorEx("ftp upload failed :", SourceClass, e);
                    return false;
                }
            }
            Logger.ErrorEx($"no file for upload found: path: {sourceDirectory} filename: {filename}", SourceClass);
            return false;
        }
        private bool FileAndPathExists(string path, string filename)
        {
            string file = Path.Combine(path, filename);
            return File.Exists(file);
        }
    }
}
