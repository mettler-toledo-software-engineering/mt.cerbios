﻿using System;
using log4net;
using mt.libraries.domain.Peripherals.DigitalIo;
using mt.libraries.peripherals.SerialDevices;
using MT.Singularity.Composition;
using MT.Singularity.Logging;
using MT.Singularity.Presentation.Model;

namespace mt.libraries.peripherals.DigitalIoDevices
{
    [Export(typeof(IDigitalIoFootPedal))]
    [InjectionBehavior(IsSingleton = true)]
    public class DigitalIoFootPedal : IDigitalIoFootPedal
    {
        
        private static readonly ILog Logger = Log4NetManager.ApplicationLogger;
        private static readonly string SourceClass = nameof(DigitalIoFootPedal);

        public event EventHandler<ExecutionResult> PedelCommandExecuted;

        public DigitalIoFootPedal(IDigitalIo digitalIoConnection)
        {
            digitalIoConnection.DigitalInputChangedEvent += OnDigitalInputChanged;
        }

        private void OnDigitalInputChanged(object sender, IDigitalInputChangedEventArgs e)
        {
            if (ActivateDevice)
            {
                if (e.Input == LeftPedalIoChannel && e.NewValue == true)
                {
                    try
                    {

                        LeftPedalTask?.Execute(null);
                        PedalCommandResult = ExecutionResult.ExecutedOk;

                    }
                    catch (Exception exception)
                    {
                        Logger.ErrorEx("could not execute left pedal task",SourceClass,exception);
                        PedalCommandResult = ExecutionResult.ExecutedWithError;
                    }
                    
                }

                if (e.Input == RightPedalIoChannel && e.NewValue == true)
                {
                    try
                    {
                        RightPedalTask?.Execute(null);
                        PedalCommandResult = ExecutionResult.ExecutedOk;
                    }
                    catch (Exception exception)
                    {
                        Logger.ErrorEx("could not execute right pedal task", SourceClass, exception);
                        PedalCommandResult = ExecutionResult.ExecutedWithError;
                    }
                    
                }
            }
            
        }

        public int LeftPedalIoChannel { get; set; }
        public int RightPedalIoChannel { get; set; }

        public ICommand LeftPedalTask { get; set; }
        public ICommand RightPedalTask { get; set; }
        public bool ActivateDevice { get; set; } = false;

        private ExecutionResult _pedalCommandExecutionResult;

        public ExecutionResult PedalCommandResult
        {
            get { return _pedalCommandExecutionResult; }
            set
            {
                _pedalCommandExecutionResult = value;
                PedelCommandExecuted?.Invoke(this, _pedalCommandExecutionResult);
            }
        }



    }
}
