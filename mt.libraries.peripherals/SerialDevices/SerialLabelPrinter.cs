﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using log4net;
using mt.libraries.domain.Peripherals.Serial;
using MT.Singularity.Composition;
using MT.Singularity.IO;
using MT.Singularity.Logging;
using MT.Singularity.Platform.Devices;
using MT.Singularity.Platform.Infrastructure;
using MT.Singularity.Serialization;

namespace mt.libraries.peripherals.SerialDevices
{
    [Export(typeof(ISerialLabelPrinter))]
    [InjectionBehavior(IsSingleton = true)]
    public class SerialLabelPrinter : ISerialLabelPrinter
    {
        private static readonly ILog Logger = Log4NetManager.ApplicationLogger;
        private static readonly string SourceClass = nameof(SerialLabelPrinter);

        public event EventHandler<PrintResult> PrintExecutedEvent;

        private ISerialInterface _serialInterface;
        private readonly IPlatformEngine _engine;
        private readonly ISerialInterfaceProviderService _interfaceProvider;
        private int _physicalPort;

        private StringSerializer _serializer;
        public SerialLabelPrinter(IPlatformEngine engine, ISerialInterfaceProviderService interfaceProvider, int physicalPort)
        {
            _engine = engine;
            _interfaceProvider = interfaceProvider;
            _physicalPort = physicalPort;
            SetupPrinter().ContinueWith(HandleException);
        }

        private void HandleException(Task SetupTask)
        {
            if (SetupTask.Exception != null)
            {
                Console.WriteLine(SetupTask.Exception);
            }
        }

        private async Task SetupPrinter()
        {
            try
            {
                _serialInterface = await _interfaceProvider.FindSerialInterfacebyPhysicalPort(_engine, _physicalPort);
                IConnectionChannel<DataSegment> serialConnectionChannel = await _serialInterface.CreateConnectionChannelAsync();
                var delimiterSerializer = new DelimiterSerializer(serialConnectionChannel, CommonDataSegments.Newline);
                _serializer = new StringSerializer(delimiterSerializer);
                _serializer.CodePage = CodePage.CP1250;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

        }

        public async Task Print(string template)
        {

            try
            {
                CreateOutBuffer(template);

                await _serializer.OpenAsync();

                await WriteBufferToChannel();

                await _serializer.CloseAsync();
                PrintExecutedEvent?.Invoke(this, PrintResult.PrintOk);

            }
            catch (Exception e)
            {
                Logger.ErrorEx("printing exception", SourceClass,e);
                PrintExecutedEvent?.Invoke(this, PrintResult.SerialChannelException);
            }

        }

        private List<string> _outbuffer;

        private void CreateOutBuffer(string template)
        {
            var buffer = template.Split(char.Parse("\n"));
            _outbuffer = new List<string>(buffer);
        }

        private async Task WriteBufferToChannel()
        {
            foreach (string line in _outbuffer)
            {
                await _serializer.WriteAsync($"{line}\n");
                Thread.Sleep(50);
            }
        }

    }
}
