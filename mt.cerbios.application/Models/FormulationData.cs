﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using mt.libraries.domain.Persistence.Models;

namespace mt.cerbios.application.Models
{
    public class FormulationData : IFormulationData, IDomainObject
    {
        public int Id { get; set; }
        public string Code { get; set; }

        public DateTime ExpiryDate { get; set; }

        public List<IFormulationItem> FormulationItems { get; set; } = new List<IFormulationItem>();

        public IFormulationItem LatestFormulationItem => FormulationItems?.Last();

        public string Lot { get; set; }

        public DateTime ProcessStarted { get; set; }

        public string Product { get; set; }

        public DateTime RetestDate { get; set; }

        public double TotalNetweight => FormulationItems?.Sum(x => x.CapturedWeight.NetWeight) ?? 0;

        public int TotalNumberOfItems => FormulationItems?.Count ?? 0;
    }
}
