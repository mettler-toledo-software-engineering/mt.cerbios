﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CsvHelper.Configuration.Attributes;
using mt.libraries.domain.Persistence.Models;

namespace mt.cerbios.application.Models
{
    public class EndDataModel : IDomainObject
    {
        [Index(0)]
        public string EndLine { get; set; } = "END";
        [Ignore]
        public int Id { get; set; }
        

    }
}
