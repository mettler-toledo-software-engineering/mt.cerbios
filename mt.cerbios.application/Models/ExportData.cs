﻿
using CsvHelper.Configuration.Attributes;
using mt.libraries.domain.Persistence.Models;
using MT.Singularity.Platform.Devices.Scale;

namespace mt.cerbios.application.Models
{
    public class ExportData : IDomainObject
    {
        [Ignore]
        public int Id { get; set; }
        [Index(0)]
        public string Code { get; set; }
        [Index(1)]
        public string Lot { get; set; }
        [Ignore]
        public WeightInformation CapturedWeight { get; set; }
        public string Netto =>  CapturedWeight == null ? "0" : CapturedWeight.NetWeight.ToString("F3");
        public string BarcodeContent { get; set; }
        public string Tara => CapturedWeight == null ? "0" : CapturedWeight.TareWeight.ToString("F3");
    }
}
