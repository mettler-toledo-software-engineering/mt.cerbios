﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MT.Singularity.Platform.Devices.Scale;

namespace mt.cerbios.application.Models
{
    public interface IFormulationData
    {
        DateTime ProcessStarted { get; set; }
        string Product { get; set; }
        string Code { get; set; }
        string Lot { get; set; }
        DateTime RetestDate { get; set; }
        DateTime ExpiryDate { get; set; }
        List<IFormulationItem> FormulationItems { get; set; }
        double TotalNetweight { get; }
        int TotalNumberOfItems { get; }
        IFormulationItem LatestFormulationItem { get; }

        

    }
}
