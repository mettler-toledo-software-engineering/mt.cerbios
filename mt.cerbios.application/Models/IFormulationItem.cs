﻿using System;
using MT.Singularity.Platform.Devices.Scale;

namespace mt.cerbios.application.Models
{
    public interface IFormulationItem
    {
        int ItemNumber { get; set; }
        string ItemNumberBarcode { get; }
        string BarcodeContent { get; }
        WeightInformation CapturedWeight { get; set; }
        DateTime CapturedTime { get; set; }

        string DisplayText { get; }
    }
}
