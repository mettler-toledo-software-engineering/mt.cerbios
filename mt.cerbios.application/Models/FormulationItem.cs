﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MT.Singularity.Platform.Devices.Scale;

namespace mt.cerbios.application.Models
{
    public class FormulationItem : IFormulationItem
    {
        private readonly string _lotNumber;
        public FormulationItem(string lotNumber)
        {
            _lotNumber = lotNumber;
        }

        public string BarcodeContent => $"{_lotNumber}{ItemNumberBarcode}";


        public DateTime CapturedTime { get; set; }
   

        public WeightInformation CapturedWeight { get; set; }


        public int ItemNumber { get; set; }


        public string ItemNumberBarcode => ItemNumber.ToString().PadLeft(4, char.Parse("0"));

        public  string DisplayText => $"{ItemNumber.ToString().PadLeft(2,char.Parse("0"))}. {CapturedTime:HH:mm:ss} G: {CapturedWeight.GrossWeight:F3}kg - N: {CapturedWeight.NetWeight:F3}kg - T: {CapturedWeight.TareWeight:F3}kg";

    }
}
