﻿using mt.cerbios.application.Models;
using mt.libraries.domain.Persistence.CSV;

namespace mt.cerbios.application.persistance
{
    public interface IExportCsvService
    {
        IGenericCsvAccess<ExportData> ExportDataAccess { get; }
        IGenericCsvAccess<EndDataModel> ExportDataEnd { get; }
    }
}