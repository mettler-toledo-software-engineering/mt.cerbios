﻿using System.Globalization;
using System.Text;
using CsvHelper.Configuration;
using mt.cerbios.application.Models;
using mt.libraries.domain.Persistence.CSV;
using mt.libraries.persistance.CSV;
using MT.Singularity.Composition;

namespace mt.cerbios.application.persistance
{
    [Export(typeof(IExportCsvService))]
    public class ExportCsvService : IExportCsvService
    {
        public ExportCsvService(string delimeter)
        {
            var config = new CsvConfiguration(CultureInfo.CurrentUICulture)
            {
                //PrepareHeaderForMatch = args => args.Header.ToLower(),

                Delimiter = delimeter,
                Encoding = Encoding.Default,
                HasHeaderRecord = false


            };
            ExportDataAccess = new GenericCsvAccess<ExportData>(config);

            ExportDataEnd = new GenericCsvAccess<EndDataModel>(config);
        }

        public IGenericCsvAccess<ExportData> ExportDataAccess { get; }
        public IGenericCsvAccess<EndDataModel> ExportDataEnd { get; }
    }
}
