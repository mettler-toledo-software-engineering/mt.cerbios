﻿namespace mt.cerbios.application.Printing
{
    public interface IPrintLabel
    {
        string GetPrintLabelTemplate();
    }
}