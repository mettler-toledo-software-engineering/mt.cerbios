﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using mt.cerbios.application.Models;

namespace mt.cerbios.application.Printing
{
    public class PrintLabelSingle : PrintLabel
    {

        public PrintLabelSingle(int numberOfLabels, IFormulationData formulationData) : base(numberOfLabels, formulationData)
        {

        }

        protected override string PrintBody()
        {
            string basebody = base.PrintBody();
            string startDate = $"A160,440,3,2,1,1,N,\"DATE        {FormulationData.LatestFormulationItem.CapturedTime:dd.MM.yyyy}\"\r\n";
            string startTime = $"A196,440,3,2,1,1,N,\"TIME          {FormulationData.LatestFormulationItem.CapturedTime:HH.mm.ss}\"\r\n";
            string grossweight = $"A472,440,3,2,1,1,N,\"GROSS     {FormulationData.LatestFormulationItem.CapturedWeight.GrossWeight.ToString("F3").PadLeft(9)} kg\"\r\n";
            string netweight =   $"A500,440,3,2,2,1,N,\"NET       {FormulationData.LatestFormulationItem.CapturedWeight.NetWeight.ToString("F3").PadLeft(9)} kg\"\r\n";
            string tare =        $"A544,440,3,2,1,1,N,\"TARE      {FormulationData.LatestFormulationItem.CapturedWeight.TareWeight.ToString("F3").PadLeft(9)} kg\"\r\n";

            string barcode = $"B196,40,0,3,2,5,80,B,\"{FormulationData.LatestFormulationItem.BarcodeContent}\"\r\n";

            string output = $"{basebody}{startDate}{startTime}{grossweight}{netweight}{tare}{barcode}";

            return output;
        }

        protected override string PrintFooter()
        {
            string baseFooter = base.PrintFooter();
            string containerNo = $"A640,440,3,2,1,1,N,\"No.       {FormulationData.LatestFormulationItem.ItemNumber.ToString().PadLeft(12)}\"\r\n";

            string output = $"{containerNo}{baseFooter}";

            return output;
        }
    }
}
