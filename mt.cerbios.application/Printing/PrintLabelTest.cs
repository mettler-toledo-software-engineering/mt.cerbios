﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using mt.cerbios.application.Models;
using MT.Singularity.Platform.Devices.Scale;

namespace mt.cerbios.application.Printing
{
    public class PrintLabelTest : PrintLabel
    {
        private WeightInformation _measuredWeight;
        public PrintLabelTest(int numberOfLabels, IFormulationData formulationData) : base(numberOfLabels, formulationData)
        {
        }

        public PrintLabelTest(WeightInformation measuredWeight)
        {
            _measuredWeight = measuredWeight;
        }

        protected override string PrintBody()
        {
            string startDate = $"A160,440,3,2,1,1,N,\"DATE        {DateTime.Now:dd.MM.yyyy}\"\r\n";
            string startTime = $"A196,440,3,2,1,1,N,\"TIME          {DateTime.Now:HH.mm.ss}\"\r\n";
            string grossweight = $"A472,440,3,2,1,1,N,\"GROSS     {_measuredWeight.GrossWeight.ToString("F3").PadLeft(9)} kg\"\r\n";
            string netweight = $"A500,440,3,2,2,1,N,\"NET       {_measuredWeight.NetWeight.ToString("F3").PadLeft(9)} kg\"\r\n";
            string tare = $"A544,440,3,2,1,1,N,\"TARE      {_measuredWeight.TareWeight.ToString("F3").PadLeft(9)} kg\"\r\n";

            string output = $"{startDate}{startTime}{grossweight}{netweight}{tare}";

            return output;
        }
        }
}
