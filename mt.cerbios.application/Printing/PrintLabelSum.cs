﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using mt.cerbios.application.Models;

namespace mt.cerbios.application.Printing
{
    public class PrintLabelSum : PrintLabel
    {
        public PrintLabelSum(int numberOfLabels, IFormulationData formulationData) : base(numberOfLabels, formulationData)
        {
        }

        protected override string PrintBody()
        {
            string basebody = base.PrintBody();
            string startDate = $"A160,440,3,2,1,1,N,\"DATE        {DateTime.Now:dd.MM.yyyy}\"\r\n";
            string startTime = $"A196,440,3,2,1,1,N,\"TIME          {DateTime.Now:HH.mm.ss}\"\r\n";
            string netTotal = $"A508,440,3,2,1,1,N,\"NET TOTAL {FormulationData?.TotalNetweight.ToString("F3").PadLeft(9)} kg\"\r\n";

            string output = $"{basebody}{startDate}{startTime}{netTotal}";

            return output;
        }

        protected override string PrintFooter()
        {
            string baseFooter =  base.PrintFooter();
            string containertotal = $"A640,440,3,2,1,1,N,\"TOTAL       {FormulationData?.TotalNumberOfItems.ToString().PadLeft(12)}\"\r\n";

            string output = $"{containertotal}{baseFooter}";

            return output;
        }
    }
}
