﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using mt.cerbios.application.Models;

namespace mt.cerbios.application.Printing
{
    public abstract class PrintLabel : IPrintLabel
    {

        protected int NumberOflabels;
        protected readonly IFormulationData FormulationData;
        public PrintLabel(int numberOfLabels, IFormulationData formulationData)
        {
            NumberOflabels = numberOfLabels;
            FormulationData = formulationData;

        }

        public PrintLabel()
        {
            
        }
        protected string PrintHeader()
        {
            string clearBuffer = "\r\nN\r\n";
            string labelSize = "q800\r\nZB\r\n";
            string codePage = "I8,A,041\r\n";

            string smallSquare = "X16,460,2,140,8\r\n";
            string bigSquare = "X16,460,2,748,8\r\n";

            string companyName = "A50,440,3,2,2,2,N,\"CERBIOS-PHARMA SA\"\r\n";
            string companyLocation = "A100,440,3,3,1,1,N,\"CH-6917 BARBENGO/LUGANO\"\r\n";

            string output = $"{clearBuffer}{codePage}{labelSize}{smallSquare}{bigSquare}{companyName}{companyLocation}";

            return output;

        }

        protected virtual string PrintBody()
        {

            string product =    $"A272,440,3,2,1,1,N,\"PRODUCT   {FormulationData.Product.PadLeft(12)}\"\r\n";
            string codeNo =     $"A308,440,3,2,1,1,N,\"CODE No.  {FormulationData.Code.PadLeft(12)}\"\r\n";
            string lot =        $"A340,440,3,2,2,1,N,\"LOT       {FormulationData.Lot.PadLeft(12)}\"\r\n";
            string retest =      "A384,440,3,2,1,1,N,\"RETEST\"\r\n";
            string retestDate = $"A404,440,3,2,1,1,N,\"DATE        {FormulationData.RetestDate:dd.MM.yyyy}\"\r\n";
            string expiry =      "A424,440,3,2,1,1,N,\"EXPIRY\"\r\n";
            string expiryDate = $"A444,440,3,2,1,1,N,\"DATE        {FormulationData.ExpiryDate:dd.MM.yyyy}\"\r\n";

            string container = "A620,440,3,2,1,1,N,\"CONTAINER\"\r\n";

            string output = $"{product}{codeNo}{lot}{retest}{retestDate}{expiry}{expiryDate}{container}";
            return output;

        }

        protected virtual string PrintFooter()
        {
            //B0 = Hex für ° in CP 1250
            string storeTemp = "A700,440,3,2,1,1,N,\"STORE AT <= -18"+"\xB0"+"C\"\r\n";
            string printCommand = $"P{NumberOflabels}\r\n";

            string output = $"{storeTemp}{printCommand}";
            return output;
        }

        public string GetPrintLabelTemplate()
        {
            string header = PrintHeader();
            string body = PrintBody();
            string footer = PrintFooter();

            string output = $"{header}{body}{footer}";

            return output;
        }
    }
}
