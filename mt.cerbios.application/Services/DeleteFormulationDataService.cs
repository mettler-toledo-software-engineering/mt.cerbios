﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using mt.cerbios.application.Models;
using mt.libraries.domain.Exceptions;
using MT.Singularity.Composition;

namespace mt.cerbios.application.Services
{
    [Export(typeof(IDeleteFormulationDataService))]
    public class DeleteFormulationDataService : IDeleteFormulationDataService
    {
        public void DeleteLastFormulationEntry(IFormulationData formulationData)
        {

            if (formulationData == null || formulationData.FormulationItems == null || formulationData.FormulationItems?.Count == 0)
            {
                throw new EmptyListException();
            }

            else
            {
                formulationData.FormulationItems.Remove(formulationData.LatestFormulationItem);
            }
            
        }
    }
}
