﻿using System;
using mt.cerbios.application.Models;
using mt.libraries.domain.Exceptions;
using MT.Singularity.Composition;

namespace mt.cerbios.application.Services
{
    [Export(typeof(IFormulationDataValidationService))]
    public class FormulationDataValidationService : IFormulationDataValidationService
    {
        private string _invalidCharacter = string.Empty;
        public bool FormulationDataIsValid(IFormulationData formulationData)
        {
            _invalidCharacter = string.Empty;
            if (string.IsNullOrWhiteSpace(formulationData.Code))
            {
                throw new MissingDataException(nameof(formulationData.Code));
            }
            if (string.IsNullOrWhiteSpace(formulationData.Lot))
            {
                throw new MissingDataException(nameof(formulationData.Lot));
            }
            if (string.IsNullOrWhiteSpace(formulationData.Product))
            {
                throw new MissingDataException(nameof(formulationData.Product));
            }

            if (formulationData.RetestDate == null || formulationData.RetestDate < DateTime.Now)
            {
                throw new InvalidDateEnteredException(nameof(formulationData.RetestDate));
            }

            if (formulationData.ExpiryDate == null || formulationData.ExpiryDate < DateTime.Now)
            {
                throw new InvalidDateEnteredException(nameof(formulationData.ExpiryDate));
            }

            if (ContainsInvalidCharacter(formulationData.Product, out _invalidCharacter))
            {
                throw new InvalidCharacterException(formulationData.Product, _invalidCharacter);
            }
            if (ContainsInvalidCharacter(formulationData.Lot, out _invalidCharacter))
            {
                throw new InvalidCharacterException(formulationData.Lot, _invalidCharacter);
            }
            return true;
        }

        private bool ContainsInvalidCharacter(string input, out string invalidCharacter)
        {
            if (input.Contains(@"\"))
            {
                invalidCharacter = @"\";
                return true;
            }
            if (input.Contains(@"/"))
            {
                invalidCharacter = @"/";
                return true;
            }
            if (input.Contains(@"?"))
            {
                invalidCharacter = @"?";
                return true;
            }
            if (input.Contains(@":"))
            {
                invalidCharacter = @":";
                return true;
            }
            if (input.Contains(@"*") )
            {
                invalidCharacter = @"*";
                return true;
            }
            if (input.Contains(@">"))
            {
                invalidCharacter = @">";
                return true;
            }
            if ( input.Contains(@"<"))
            {
                invalidCharacter = @"<";
                return true;
            }
            if (input.Contains("\""))
            {
                invalidCharacter = "\"";
                return true;
            }
            if (input.Contains(@"|"))
            {
                invalidCharacter = @"|";
                return true;
            }

            invalidCharacter = string.Empty;
            return false;
        }
    }
}
