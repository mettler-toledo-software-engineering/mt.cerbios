﻿using mt.cerbios.application.Models;

namespace mt.cerbios.application.Services
{
    public interface IDeleteFormulationDataService
    {
        void DeleteLastFormulationEntry(IFormulationData formulationData);
    }
}