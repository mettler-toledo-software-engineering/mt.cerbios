﻿namespace mt.cerbios.application.Services
{
    public interface ISystemUpdateService
    {
        bool ExecuteUpdate();
        bool SystemUpdateFileExists();
    }
}