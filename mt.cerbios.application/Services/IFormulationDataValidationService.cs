﻿using mt.cerbios.application.Models;

namespace mt.cerbios.application.Services
{
    public interface IFormulationDataValidationService
    {
        bool FormulationDataIsValid(IFormulationData formulationData);
    }
}