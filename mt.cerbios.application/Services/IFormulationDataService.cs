﻿namespace mt.cerbios.application.Services
{
    public interface IFormulationDataService
    {
        IDeleteFormulationDataService DeleteFormulationData { get; }
        IFormulationDataValidationService FormulationValidation { get; }
    }
}