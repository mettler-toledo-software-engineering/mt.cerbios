﻿using MT.Singularity.Composition;

namespace mt.cerbios.application.Services
{
    [Export(typeof(IFormulationDataService))]
    public class FormulationDataService : IFormulationDataService
    {

        public FormulationDataService(IDeleteFormulationDataService deleteFormulationData, IFormulationDataValidationService formulationDataValidation)
        {
            DeleteFormulationData = deleteFormulationData;
            FormulationValidation = formulationDataValidation;
        }

        public IDeleteFormulationDataService DeleteFormulationData { get; }
        public IFormulationDataValidationService FormulationValidation { get; }


    }
}
