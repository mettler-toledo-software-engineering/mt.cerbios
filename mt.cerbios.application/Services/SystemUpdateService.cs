﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using log4net.Repository.Hierarchy;
using MT.Singularity.Logging;
using MT.Singularity.Platform;

namespace mt.cerbios.application.Services
{
    public class SystemUpdateService : ISystemUpdateService
    {

        private static readonly string DestinationPath = SingularityEnvironment.ServiceDirectory;
        private static readonly string SourceDirectory = @"D:\systemupdate\";
        private List<string> _availableUpdateFiles = new List<string>();


        public bool SystemUpdateFileExists()
        {
            if (CheckDirectoriesExists())
            {
                _availableUpdateFiles = Directory.EnumerateFiles(SourceDirectory).Select(Path.GetFileName).Where(x => x.Contains(SingularityEnvironment.AppPackageExtension)).ToList();
                bool hasFiles = _availableUpdateFiles.Count > 0;
                return hasFiles;
            }

            return false;
        }

        public bool ExecuteUpdate()
        {
            bool result = false;
            if (SystemUpdateFileExists())
            {
                foreach (string updateFile in _availableUpdateFiles)
                {
                    result = CopyFile(updateFile);
                    if (result == false)
                    {
                        break;
                    }
                }

            }

            return result;
        }

        private bool CopyFile(string filename)
        {
            string sourcefile = Path.Combine(SourceDirectory, filename);
            string destinationfile = Path.Combine(DestinationPath, filename);
            bool result = false;
            try
            {
                File.Copy(sourcefile, destinationfile, true);
                result = true;
            }
            catch (IOException)
            {

            }

            return result;
        }


        private bool CheckDirectoriesExists()
        {
            bool result = true;
            if (Directory.Exists(DestinationPath) == false)
            {
                result = CreateDirectory(DestinationPath);

            }

            if (Directory.Exists(SourceDirectory) == false)
            {
                result = CreateDirectory(SourceDirectory);
            }

            return result;
        }

        private bool CreateDirectory(string path)
        {
            bool result = false;
            try
            {
                Directory.CreateDirectory(path);
                result = true;
            }
            catch (IOException)
            {
                result = false;
            }

            return result;
        }
    }
}
