﻿using mt.libraries.domain.Network.FTP;

namespace mt.cerbios.application.FTP
{
    public interface IFtpExportService
    {
        IFtpFileTransferService FtpTransfer { get; }
    }
}