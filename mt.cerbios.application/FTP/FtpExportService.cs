﻿
using mt.libraries.Authentication.Credentials;
using mt.libraries.domain.Authentication;
using mt.libraries.domain.Network.FTP;
using mt.libraries.network.FTP;

namespace mt.cerbios.application.FTP
{
    public class FtpExportService : IFtpExportService
    {
        public FtpExportService(string ftpServerAndFolder, string username, string password)
        {
            IFtpServer ftpServer = new FtpServer(ftpServerAndFolder);
            IUser user = new User{Username = username, Password = password};
            FtpTransfer = new FtpFileTransferService(ftpServer, user);
        }

        public IFtpFileTransferService FtpTransfer { get; }
    }
}
