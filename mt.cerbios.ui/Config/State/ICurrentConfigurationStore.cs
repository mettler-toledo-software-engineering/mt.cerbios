﻿namespace mt.cerbios.ui.Config.State
{
    public interface ICurrentConfigurationStore
    {
        Configuration CurrentConfiguration { get; set; }
    }
}