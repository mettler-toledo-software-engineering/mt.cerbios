﻿using System;

using System.Collections.Generic;
using System.Text;

namespace mt.cerbios.ui.Config.State
{

    public class CurrentConfigurationStore : ICurrentConfigurationStore
    {
        public Configuration CurrentConfiguration { get; set; }
    }
}
