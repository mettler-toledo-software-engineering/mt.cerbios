﻿using System.ComponentModel;
using mt.cerbios.ui.State;
using mt.libraries.peripherals.SerialDevices;
using MT.Singularity.Composition;

namespace mt.cerbios.ui.Config.State
{
    [Export(typeof(IConfigurationUpdateService))]
    [InjectionBehavior(IsSingleton = true)]
    public class ConfigurationUpdateService : IConfigurationUpdateService
    {

        private ICurrentConfigurationStore _currentConfiguration;
        private ISerialLabelPrinterStore _serialLabelPrinterStore;
        
        public ConfigurationUpdateService(ICurrentConfigurationStore currentConfiguration, ISerialLabelPrinterStore labelPrinterStore)
        {
            _currentConfiguration = currentConfiguration;
            _serialLabelPrinterStore = labelPrinterStore;

            ConfigurationLoaded();
        }

        private void ConfigurationLoaded()
        {


                _currentConfiguration.CurrentConfiguration.PropertyChanged += UpdateConfiguration;

        }

        private void UpdateConfiguration(object sender, PropertyChangedEventArgs e)
        {

            if (e.PropertyName == "PrinterPort")
            {
                _serialLabelPrinterStore.ConfiguredSerialLabelPrinter = new SerialLabelPrinter(_serialLabelPrinterStore.PlatformEngine, _serialLabelPrinterStore.SerialInterfaceProviderService,_currentConfiguration.CurrentConfiguration.PrinterPort);

            }
        }
    }
}
