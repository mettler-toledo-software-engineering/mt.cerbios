﻿using System;

using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using mt.cerbios.application.FTP;
using mt.cerbios.application.Services;
using mt.cerbios.ui.Infrastructure;
using MT.Singularity.Collections;
using MT.Singularity.Composition;
using MT.Singularity.Logging;
using MT.Singularity.Platform.CommonUX.Setup.Impl;
using MT.Singularity.Platform.CommonUX.Setup.ViewModels;
using MT.Singularity.Platform.Infrastructure;
using MT.Singularity.Presentation;

namespace mt.cerbios.ui.Config
{
    class SettingsSubNode : GroupSetupMenuItem
    {
        private readonly Configuration _configuration;

        public SettingsSubNode(SetupMenuContext context, IComponents customerComponent, Configuration configuration)
            : base(context, new TitleAndSubtitle(Localization.GetTranslationModule(), (int)Localization.Key.GeneralSettings), configuration, customerComponent)
        {
            _configuration = configuration;

        }

        public override Task ShowChildrenAsync()
        {
            try
            {
                // Titles for the my Setup parameters


                var printerPortTitle = new TitleAndSubtitle(Localization.GetTranslationModule(), (int)Localization.Key.PrinterPort);
                var printerPortTarget = new TextSetupMenuItem(_context, printerPortTitle, _configuration, "PrinterPort");

                var ftpServerTitle = new TitleAndSubtitle(Localization.GetTranslationModule(), (int)Localization.Key.FtpServerAndFolder);
                var ftpServerTarget = new TextSetupMenuItem(_context, ftpServerTitle, _configuration, "FtpServerAndFolder");

                var ftpUsernameTitle = new TitleAndSubtitle(Localization.GetTranslationModule(), (int)Localization.Key.FtpUserName);
                var ftpUsernameTarget = new TextSetupMenuItem(_context, ftpUsernameTitle, _configuration, "FtpUsername");

                var ftpPasswordTitle = new TitleAndSubtitle(Localization.GetTranslationModule(), (int)Localization.Key.FtpUserPassword);
                var ftpPasswordTarget = new TextSetupMenuItem(_context, ftpPasswordTitle, _configuration, "FtpPassword");

                var exportFileSeperatorTitle = new TitleAndSubtitle(Localization.GetTranslationModule(), (int)Localization.Key.ExportFileSeparator);
                var exportFileSeperatorTarget = new TextSetupMenuItem(_context, exportFileSeperatorTitle, _configuration, "ExportFileSeparator");

                var retestDurationTitle = new TitleAndSubtitle(Localization.GetTranslationModule(), (int)Localization.Key.RetestDuration);
                var retestDurationTarget = new TextSetupMenuItem(_context, retestDurationTitle, _configuration, "RetestDuration");

                var expiryDurationTitle = new TitleAndSubtitle(Localization.GetTranslationModule(), (int)Localization.Key.ExpiryDate);
                var expiryDurationTarget = new TextSetupMenuItem(_context, expiryDurationTitle, _configuration, "ExpiryDuration");

                var testResultTitle = new TitleAndSubtitle(Localization.GetTranslationModule(), (int)Localization.Key.TestResult);
                var testResultTarget = new TextBlockSetupMenuItem(_context, testResultTitle, _configuration, "TestResult");


                ftpPasswordTarget.PasswordChar = char.Parse("*");

                //buttons können nicht in gruppen gelegt werden sondern müssen direkt an das child
                var updateButton = new ImageButtonSetupMenuItem(_context, Globals.FlashDiskImage, new Color(0xFF, 0x00, 0x00, 0x00), Localization.GetTranslationModule(), (int)Localization.Key.Update, 20, SystemUpdate);
                var ftpButton = new ImageButtonSetupMenuItem(_context, Globals.NetworkImage, new Color(0xFF, 0x00, 0x00, 0x00), Localization.GetTranslationModule(), (int)Localization.Key.FtpTest, 20, FtpTest);


                var rangeGroup1 = new GroupedSetupMenuItems(_context,  printerPortTarget, retestDurationTarget, expiryDurationTarget);
                var rangeGroup2 = new GroupedSetupMenuItems(_context,  ftpServerTarget, ftpUsernameTarget,ftpPasswordTarget );
                var rangeGroup3 = new GroupedSetupMenuItems(_context,  exportFileSeperatorTarget, testResultTarget);
                

                Children = Indexable.ImmutableValues<SetupMenuItem>(rangeGroup1, rangeGroup2, rangeGroup3 ,updateButton, ftpButton);


            }
            catch (Exception ex)
            {
                Log4NetManager.ApplicationLogger.Error("Error Printer Setup Node.ShowChildrenAsync", ex);
            }

            return TaskEx.CompletedTask;
        }

        private void FtpTest()
        {
            IFtpExportService ftpexport = new FtpExportService(_configuration.FtpServerAndFolder, _configuration.FtpUsername, _configuration.FtpPassword);

            bool result = ftpexport.FtpTransfer.Updload("test", Globals.Environment.DataDirectory);

            if (result == true)
            {
                _configuration.TestResult = "ftp upload success!";
            }
            else
            {
                _configuration.TestResult = "ftp upload failed!";
            }
        }

        private void SystemUpdate()
        {
            ISystemUpdateService systemUpdate = new SystemUpdateService();
            
            var success = systemUpdate.ExecuteUpdate();

            if (success == true)
            {
                _configuration.TestResult = "Update copied.";
            }
            else
            {
                _configuration.TestResult = "Update failed!";
            }
        }
    }
}
