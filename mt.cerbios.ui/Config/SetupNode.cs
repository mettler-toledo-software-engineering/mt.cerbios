﻿using System;

using System.Collections.Generic;
using System.Text;
using MT.Singularity.Collections;
using MT.Singularity.Composition;
using MT.Singularity.Logging;
using MT.Singularity.Platform.CommonUX.Setup.Impl;
using MT.Singularity.Platform.CommonUX.Setup.ViewModels;

namespace mt.cerbios.ui.Config
{
    [Export(typeof(CustomerGroupSetupMenuItem))]
    public class SetupNode : CustomerGroupSetupMenuItem
    {
        public SetupNode(SetupMenuContext context) : base(context, new TitleAndSubtitle(Localization.GetTranslationModule(), (int)Localization.Key.MainSetupNode))
        {
        }
        private static readonly string SourceClass = nameof(SetupNode);
        /// <summary>
        /// Show the children of this group. This will create the children.
        /// </summary>
        /// <returns></returns>
        public override async System.Threading.Tasks.Task ShowChildrenAsync()
        {
            try
            {
                var customerComponent = _context.CompositionContainer.Resolve<IComponents>();
                Configuration customerConfiguration = await customerComponent.GetConfigurationToChangeAsync();
                //  hier werden die 1-n subnodes für einen main setup node erstellt.
                var settingsSubNode = new SettingsSubNode(_context, customerComponent, customerConfiguration);

                Children = Indexable.ImmutableValues<SetupMenuItem>(settingsSubNode);

            }
            catch (Exception ex)
            {
                Log4NetManager.ApplicationLogger.ErrorEx("setup node crashed", SourceClass, ex);
            }
        }
    }
}
