﻿using System;

using System.Collections.Generic;
using System.Text;
using MT.Singularity.Composition;
using MT.Singularity.Platform.Configuration;
using MT.Singularity.Platform.UserManagement;

namespace mt.cerbios.ui.Config
{
    [Export(typeof(IComponents))]
    [Export(typeof(IConfigurable))]
    [InjectionBehavior(IsSingleton = true)]
    public class Components : ConfigurationStoreConfigurable<Configuration>, IComponents
    {
        public Components(IConfigurationStore configurationStore,
            ISecurityService securityService, CompositionContainer compositionContainer, bool traceChanges = true)
            : base(configurationStore, securityService, compositionContainer, traceChanges)
        {
        }
        //identifikationsschlüssel im Datenbank file, welches die Konfiguration abspeichert.
        public override string ConfigurationSelector
        {
            get { return "cerbiosConfiguration"; }
        }

        public override string FriendlyName
        {
            get { return "cerbios Configuration"; }
        }

    }
}
