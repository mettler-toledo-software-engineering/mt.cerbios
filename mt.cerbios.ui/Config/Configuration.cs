﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using MT.Singularity.Components;
using MT.Singularity.Platform.Configuration;

namespace mt.cerbios.ui.Config
{
    [Component]
    public class Configuration : ComponentConfiguration
    {

        [Setting(WritePermissions = Permissions.UntilSupervisorId)]
        [Component]
        [DefaultValue(3)]
        public virtual int PrinterPort
        {
            get { return (int)GetLocal(); }
            set { SetLocal(value); }
        }


        [Setting(WritePermissions = Permissions.UntilSupervisorId)]
        [Component]
        [DefaultValue("")]
        public virtual string FtpServerAndFolder
        {
            get { return (string)GetLocal(); }
            set { SetLocal(value); }
        }

        [Setting(WritePermissions = Permissions.UntilSupervisorId)]
        [Component]
        [DefaultValue("")]
        public virtual string FtpUsername
        {
            get { return (string)GetLocal(); }
            set { SetLocal(value); }
        }
        [Setting(WritePermissions = Permissions.UntilSupervisorId)]
        [Component]
        [DefaultValue("")]
        public virtual string FtpPassword
        {
            get { return (string)GetLocal(); }
            set { SetLocal(value); }
        }



        [Setting(WritePermissions = Permissions.UntilSupervisorId)]
        [Component]
        [DefaultValue(";")]
        public virtual string ExportFileSeparator
        {
            get { return (string)GetLocal(); }
            set { SetLocal(value); }
        }

        [Setting(WritePermissions = Permissions.UntilSupervisorId)]
        [Component]
        [DefaultValue(180)]
        public virtual int RetestDuration
        {
            get { return (int)GetLocal(); }
            set { SetLocal(value); }
        }

        [Setting(WritePermissions = Permissions.UntilSupervisorId)]
        [Component]
        [DefaultValue(720)]
        public virtual int ExpiryDuration
        {
            get { return (int)GetLocal(); }
            set { SetLocal(value); }
        }

        [Setting(WritePermissions = Permissions.UntilSupervisorId)]
        [Component]
        [DefaultValue("")]
        public virtual string TestResult
        {
            get { return (string)GetLocal(); }
            set { SetLocal(value); }
        }



    }
}
