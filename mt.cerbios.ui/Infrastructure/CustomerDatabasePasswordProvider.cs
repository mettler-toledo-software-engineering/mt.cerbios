﻿using System;

using System.Collections.Generic;
using System.Text;
using MT.Singularity.Composition;
using MT.Singularity.Platform.Infrastructure;

namespace mt.cerbios.ui.Infrastructure
{
    /// <summary>
    /// Provides the password for the customer database 
    /// Please keep the <see cref="ExportAttribute"/> and the <see cref="InjectionBehaviorAttribute"/> on the 
    /// implementation class of the <see cref="ICustomerDatabasePasswordProvider"/>, 
    /// it will help Singularity to find your customer database password provider class.
    /// </summary>
    [Export(typeof(ICustomerDatabasePasswordProvider))]
    [InjectionBehavior(IsSingleton = true)]
    public class CustomerDatabasePasswordProvider : ICustomerDatabasePasswordProvider
    {
        /// <summary>
        /// Gets the password of the customer database.
        /// You could simply return a password for your database. 
        /// if you want to keep your password safely, you could protect the password by a C++ dll file to get rid of the 
        /// decompiling risk.
        /// </summary>
        public string GetPassword()
        {
            return "111";
        }
    }
}
