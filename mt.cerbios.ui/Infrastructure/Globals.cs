﻿using System;

using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using MT.Singularity.Platform;
using MT.Singularity.Presentation;
using MT.Singularity.Presentation.Drawing;

namespace mt.cerbios.ui.Infrastructure
{
    public static class Globals
    {

        public static readonly SingularityEnvironment Environment = new SingularityEnvironment("mt.cerbios.ui");

        public static string ProjectNumber = "P21041401";
        public static string ProgVersionStr(bool withRevision)
        {
            Version version = Assembly.GetExecutingAssembly().GetName().Version;
            string vers = "V" + version.Major.ToString() + "." + version.Minor.ToString() + "." + version.Build.ToString();
            if (withRevision)
            {
                vers += "." + version.Revision.ToString();
#if DEBUG
                vers += " [DEBUG]";
#endif
            }

            return vers;
        }

        public static string IND930USBDrive = "D:\\";
        public static string DegradedModeUserName = "DegradedModeUser";
        public static string UsbBackupBaseDirectory = Path.Combine(IND930USBDrive, "Backup");
        public static string SystemUpdateSourceDirectory = Path.Combine(IND930USBDrive, "systemupdate");
        public static string SystemUpdateTargetDirectory = SingularityEnvironment.ServiceDirectory;

        public static string BarcodeScannerName = "Barcode Scanner";
        public static readonly SolidColorBrush LightGray = Colors.LightGrayBrush;
        public static readonly Color Red = Colors.Red;
        public static readonly Color Green = Colors.MediumGreen;
        public static readonly SolidColorBrush DarkBlue = Colors.DarkBlueBrush;
        public static readonly Color White = Colors.White;

        public static readonly int TextBlockHeight = 50;
        public static readonly int TextBlockWidth = 500;
        public static readonly int HeaderFontSize = 28;
        public static readonly int BodyFontSize = 24;
        public static readonly int InputFontSize = 36;
        public static readonly Thickness DefaultPadding = new Thickness(5);
        public static readonly Thickness BottomMargin5 = new Thickness(0,0,0,5);
        public static readonly Thickness BottomMargin10 = new Thickness(0,0,0,10);
        public static readonly Thickness BottomMargin15 = new Thickness(0,0,0,15);

        public static readonly string FlashDiskImage = "embedded://mt.cerbios.ui/mt.cerbios.ui.images.FlashDisk.al8";
        public static readonly string StartImage = "embedded://mt.cerbios.ui/mt.cerbios.ui.images.Start.al8";
        public static readonly string ClearImage = "embedded://mt.cerbios.ui/mt.cerbios.ui.images.clear_all.al8";
        public static readonly string ExitImage = "embedded://mt.cerbios.ui/mt.cerbios.ui.images.Exit.al8";
        public static readonly string PrintImage = "embedded://mt.cerbios.ui/mt.cerbios.ui.images.Print.al8";
        public static readonly string CerbiosImage = "embedded://mt.cerbios.ui/mt.cerbios.ui.images.cerbios.png";
        public static readonly string NetworkImage = "embedded://mt.cerbios.ui/mt.cerbios.ui.images.Network.al8";
        public static readonly string DeleteImage = "embedded://mt.cerbios.ui/mt.cerbios.ui.images.Delete.al8";

        public static readonly int NumberOfPrintLabels = 2;
        public static readonly int GetWeightTimeOutInMs = 1000;

    }
}
