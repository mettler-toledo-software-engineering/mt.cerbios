﻿using System;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using log4net;
using mt.cerbios.application.Models;
using mt.cerbios.ui.Config;
using mt.cerbios.ui.Config.State;
using mt.cerbios.ui.State;
using mt.cerbios.ui.Views;
using mt.libraries.domain.Peripherals.DigitalIo;
using mt.libraries.domain.Peripherals.Serial;
// ReSharper disable once RedundantUsingDirective
using MT.Singularity.Composition;
using MT.Singularity.Logging;
using MT.Singularity.Platform.CommonUX.Infrastructure;
using MT.Singularity.Platform.Configuration;
using MT.Singularity.Platform.Devices.Scale;
using MT.Singularity.Platform.Infrastructure;


namespace mt.cerbios.ui.Infrastructure
{
    /// <summary>
    /// Initializes and runs the application.
    /// </summary>
    internal class Bootstrapper : ClientBootstrapperBase
    {
        private static readonly ILog Logger = Log4NetManager.ApplicationLogger;
        private static readonly string SourceClass = nameof(Bootstrapper);
        /// <summary>
        /// Initializes a new instance of the <see cref="Bootstrapper"/> class.
        /// </summary>
        public Bootstrapper()
            : base(Assembly.GetExecutingAssembly())
        {
        }

        /// <summary>
        /// Gets a value indicating whether it need to create customer database.
        /// </summary>
        protected override bool NeedToCreateCustomerDatabase
        {
            get { return true; }
        }

        /// <summary>
        /// Initializes the application in this method. This method is called after all
        /// services have been started and before the <see cref="T:MT.Singularity.Platform.Infrastructure.IShell" /> is shown.
        /// </summary>
        protected override async void InitializeApplication()
        {
            base.InitializeApplication();

            SetCustomCulture();

            await InitializeCustomerService();
        }

        /// <summary>
        /// Initializes the customer service.
        /// Put all customer services or components initialization code in this method.
        /// </summary>
        private async Task InitializeCustomerService()
        {
            try
            {


                CompositionContainer.EnableRecursiveResolution();

                var engine = CompositionContainer.Resolve<IPlatformEngine>();
                var securityService = await engine.GetSecurityServiceAsync();
                var configurationStore = CompositionContainer.Resolve<IConfigurationStore>();


                var customerComponent = new Components(configurationStore, securityService, CompositionContainer);
                CompositionContainer.AddInstance<IComponents>(customerComponent);


                Logger.InfoEx("registering io form bootstrapper", SourceClass);
                //var digitalIo = new DigitalIo(engine);


                var formulationData = new FormulationData
                {
                    Product = "EGSM",
                    Code = "650001-00-02",
                    Lot = "K00001",
                    ProcessStarted = DateTime.Now,
                    ExpiryDate = DateTime.Now.AddYears(2)
                    
                };

                CompositionContainer.AddAssembly(Assembly.Load("mt.cerbios.application"));
                CompositionContainer.AddAssembly(Assembly.Load("mt.libraries.domain"));
                CompositionContainer.AddAssembly(Assembly.Load("mt.libraries.interfaces"));
                CompositionContainer.AddAssembly(Assembly.Load("mt.libraries.network"));
                CompositionContainer.AddAssembly(Assembly.Load("mt.libraries.authentication"));
                CompositionContainer.AddAssembly(Assembly.Load("mt.libraries.peripherals"));
                CompositionContainer.AddAssembly(Assembly.Load("mt.libraries.persistance"));
                
                

                var currentConfiguration = customerComponent.GetConfigurationAsync().GetAwaiter().GetResult();
                var currentConfigurationStore = new CurrentConfigurationStore {CurrentConfiguration = currentConfiguration};

                CompositionContainer.AddInstance<ICurrentConfigurationStore>(currentConfigurationStore);
                CompositionContainer.AddInstance<IFormulationDataStore>(new FormulationDataStore(formulationData));

                var serialPrinterStore = new SerialLabelPrinterStore(engine, CompositionContainer.Resolve<ISerialInterfaceProviderService>(), currentConfiguration.PrinterPort);


                var footpedal = new DigitalFootPedalStore(CompositionContainer.Resolve<IDigitalIoFootPedal>(), CompositionContainer.Resolve<IScaleService>(), CompositionContainer.Resolve<IFormulationDataStore>(), serialPrinterStore);
                CompositionContainer.AddInstance<IDigitalFootPedalStore>(footpedal);


                CompositionContainer.AddInstance(typeof(WeightWindowControl));
                //CompositionContainer.AddInstance<IConfigurationUpdateService>(new ConfigurationUpdateService(currentConfigurationStore, serialPrinterStore));
                CompositionContainer.AddInstance<ISerialLabelPrinterStore>(serialPrinterStore);

                CompositionContainer.AddInstance<IFormulationData>(formulationData);
                

                GetType().Assembly.GetTypes()
                    .Where(type => type.IsClass)
                    .Where(type => type.Name.EndsWith("View"))
                    .ToList()
                    .ForEach(viewModelType => CompositionContainer.AddInstance(viewModelType));

                GetType().Assembly.GetTypes()
                    .Where(type => type.IsClass)
                    .Where(type => type.Name.EndsWith("ViewModel"))
                    .ToList()
                    .ForEach(viewModelType => CompositionContainer.AddInstance(viewModelType));

            }
            catch (Exception ex)
            {
                Log4NetManager.ApplicationLogger.Error("Error Bootstrapper", ex);
                //throw;
            }

        }

        private void SetCustomCulture()
        {
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)Thread.CurrentThread.CurrentUICulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";

            Thread.CurrentThread.CurrentUICulture = customCulture;
        }
    }
}
