﻿using System;

using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using mt.cerbios.application.Models;
using mt.cerbios.application.Printing;
using mt.cerbios.ui.Infrastructure;
using mt.cerbios.ui.State;
using mt.cerbios.ui.ViewModels;
using mt.libraries.domain.Peripherals.Serial;
using MT.Singularity.Platform.Devices.Scale;

namespace mt.cerbios.ui.Commands
{
    public class PrintTestLabelCommand : PrintLabelBaseCommand
    {

        public PrintTestLabelCommand(ISerialLabelPrinter serialLabelPrinter, IScaleService scaleService) : base(serialLabelPrinter, scaleService)
        {
        }



        protected override string GetPrinterTemplate(WeightInformation weight)
        {
            IPrintLabel printLabel = new PrintLabelTest(weight); ;

            string printtemplate = printLabel.GetPrintLabelTemplate();
            return printtemplate;
        }
    }
}
