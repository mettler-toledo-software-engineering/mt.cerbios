﻿using System;

using System.Collections.Generic;
using System.Text;
using mt.cerbios.application.Models;
using mt.cerbios.ui.Config.State;
using mt.cerbios.ui.State;
using mt.cerbios.ui.ViewModels;
using MT.Singularity.Presentation.Model;

namespace mt.cerbios.ui.Commands
{
    public class ResetDataFieldsCommand : ICommand
    {
        private IFormulationDataStore _formulationDataStore;
        private readonly ICurrentConfigurationStore _currentConfiguration;
        public ResetDataFieldsCommand(IFormulationDataStore formulationDataStore, ICurrentConfigurationStore currentConfiguration)
        {
            _formulationDataStore = formulationDataStore;
            _currentConfiguration = currentConfiguration;
        }
        public event EventHandler CanExecuteChanged;

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            _formulationDataStore.CurrentFormulation = new FormulationData()
            {
                Product = "EGSM",
                Code = "650001-00-02",
                ProcessStarted = DateTime.Now,
                RetestDate = DateTime.Now.AddDays(_currentConfiguration.CurrentConfiguration.RetestDuration),
                FormulationItems = new List<IFormulationItem>(),
                ExpiryDate = DateTime.Now.AddDays(_currentConfiguration.CurrentConfiguration.ExpiryDuration)

            };
        }
    }
}
