﻿using System;

using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using log4net;
using mt.cerbios.ui.ViewModels;
using mt.libraries.persistance.CSV;
using MT.Singularity.Logging;
using MT.Singularity.Platform.Devices.Scale;
using MT.Singularity.Presentation.Model;

namespace mt.cerbios.ui.Commands
{
    public class TareScaleCommand : AsyncCommandBase
    {
        private static readonly ILog Logger = Log4NetManager.ApplicationLogger;
        private static readonly string SourceClass = nameof(TareScaleCommand);

        private readonly IScaleService _scaleService;
        public TareScaleCommand(IScaleService scaleService)
        {
            _scaleService = scaleService;
        }


        public override async Task ExecuteAsync(object parameter)
        {
            try
            {
                await _scaleService.SelectedScale.TareAsync();
            }
            catch (Exception e)
            {
                Logger.ErrorEx("Tare failed: ",SourceClass,e);
            }
            

        }
    }
}
