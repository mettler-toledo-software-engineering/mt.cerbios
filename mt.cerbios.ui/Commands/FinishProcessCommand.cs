﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using mt.cerbios.application.FTP;
using mt.cerbios.application.Models;
using mt.cerbios.application.persistance;
using mt.cerbios.ui.Config.State;
using mt.cerbios.ui.Infrastructure;
using mt.cerbios.ui.State;
using mt.cerbios.ui.ViewModels;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Model;

namespace mt.cerbios.ui.Commands
{
    public class FinishProcessCommand : ICommand
    {
        private readonly ProcessViewModel _processViewModel;
        private IFormulationDataStore _formulationDataStore;
        private ICurrentConfigurationStore _currentConfiguration;
        private IExportCsvService _csvService;
        private IFtpExportService _ftpExport;
        private string _csvFileName = string.Empty;


        public FinishProcessCommand(ProcessViewModel processViewModel, IFormulationDataStore formulationData, ICurrentConfigurationStore currentConfiguration)
        {
            _processViewModel = processViewModel;
            _formulationDataStore = formulationData;
            _currentConfiguration = currentConfiguration;
            _csvService = new ExportCsvService(currentConfiguration.CurrentConfiguration.ExportFileSeparator);
            _ftpExport = new FtpExportService(currentConfiguration.CurrentConfiguration.FtpServerAndFolder, currentConfiguration.CurrentConfiguration.FtpUsername, currentConfiguration.CurrentConfiguration.FtpPassword);

        }
        public event EventHandler CanExecuteChanged;

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            MessageBox.Show(_processViewModel.ParentVisual, Localization.Get(Localization.Key.FinishProcess), "", MessageBoxButtons.YesNo, MessageBoxIcon.Question, FinishProcess);
        }

        private void FinishProcess(DialogResult result)
        {
            if (result == DialogResult.Yes)
            {
                GenerateCsvFileName();

                bool writeResult = WriteItemsToDrive();

                var csvFiles = GetAllCsvFiles();

                foreach (string csvFile in csvFiles)
                {
                    bool uploadResult = TransferFilesToNetwork(csvFile);

                    DeleteLocalFile(csvFile, uploadResult);
                }



                ResetCurrentFormulation();

                _processViewModel.ParentPage.NavigationService.Back();
            }
        }

        private bool WriteItemsToDrive()
        {
            bool result = false;
            List<ExportData> epxportData = new List<ExportData>();

            if (_formulationDataStore.CurrentFormulation.FormulationItems?.Count > 0)
            {
                foreach (IFormulationItem formulationItem in _formulationDataStore.CurrentFormulation.FormulationItems)
                {
                    ExportData exportItem = new ExportData
                    {
                        BarcodeContent = formulationItem.BarcodeContent,
                        CapturedWeight = formulationItem.CapturedWeight,
                        Lot = _formulationDataStore.CurrentFormulation.Lot,
                        Code = _formulationDataStore.CurrentFormulation.Code
                    };
                    epxportData.Add(exportItem);
                }

                try
                {
                    result = _csvService.ExportDataAccess.SaveEntries(Globals.Environment.DataDirectory, _csvFileName, epxportData);
                    if (result == true)
                    {
                        result = _csvService.ExportDataEnd.SaveEntry(Globals.Environment.DataDirectory, _csvFileName, new EndDataModel{EndLine = "END"});
                    }
                }
                catch (Exception e)
                {
                    _processViewModel.ErrorMessage = $"Writing CSV File failed: {e.Message}";
                }
            }

            return result;
        }

        private bool TransferFilesToNetwork(string csvFile)
        {

            bool uploadResult = false;

            try
            {
                uploadResult = _ftpExport.FtpTransfer.Updload(csvFile, Globals.Environment.DataDirectory);
            }
            catch (Exception e)
            {
                _processViewModel.ErrorMessage = $"Upload to FTP Failed: {e.Message}";
            }

            return uploadResult;
        }

        private void DeleteLocalFile(string csvFile, bool uploadResult)
        {

            if (uploadResult == true)
            {
                try
                {
                    string localFile = Path.Combine(Globals.Environment.DataDirectory, csvFile);
                    if (File.Exists(localFile))
                    {
                        File.Delete(localFile);
                    }

                    _csvFileName = string.Empty;

                }
                catch (Exception e)
                {
                    _processViewModel.ErrorMessage = $"local File could not be deleted: {e.Message}";
                }
            }
        }

        private void ResetCurrentFormulation()
        {
            _formulationDataStore.CurrentFormulation = new FormulationData()
            {
                Product = "EGSM",
                Code = "650001-00-02",
                ProcessStarted = DateTime.Now,
                RetestDate = DateTime.Now.AddDays(_currentConfiguration.CurrentConfiguration.RetestDuration),
                FormulationItems = new List<IFormulationItem>(),
                ExpiryDate = DateTime.Now.AddDays(_currentConfiguration.CurrentConfiguration.ExpiryDuration)

            };
        }

        private void GenerateCsvFileName()
        {
            try
            {
                _csvFileName = $"MT{DateTime.Now:ddMMyy}{_formulationDataStore.CurrentFormulation.Lot}{_formulationDataStore.CurrentFormulation.Code}.TXT";
            }
            catch (Exception)
            {
                _csvFileName = string.Empty;
            }
        }

        private IEnumerable<string> GetAllCsvFiles()
        {
            IEnumerable<string> files = Directory.EnumerateFiles(Globals.Environment.DataDirectory,"*.TXT").Select(Path.GetFileName);
            
            return files;
        }

    }
}
