﻿using System;

using System.Collections.Generic;
using System.Text;
using mt.cerbios.application.Printing;
using mt.cerbios.ui.Infrastructure;
using mt.cerbios.ui.State;
using mt.libraries.domain.Peripherals.Serial;
using MT.Singularity.Platform.Devices.Scale;

namespace mt.cerbios.ui.Commands
{
    public class RePrintCommand : PrintLabelBaseCommand
    {
        public RePrintCommand(IFormulationDataStore formulationData, ISerialLabelPrinter serialLabelPrinter, IScaleService scaleService) : base(formulationData, serialLabelPrinter, scaleService)
        {
        }

        protected override string GetPrinterTemplate(WeightInformation weight)
        {
            IPrintLabel printLabel = new PrintLabelSingle(Globals.NumberOfPrintLabels, _formulationDataStore.CurrentFormulation);


            string printtemplate = printLabel.GetPrintLabelTemplate();
            return printtemplate;
        }

        protected override void AddNewFormulationItem(WeightInformation weight)
        {
        }
    }
}
