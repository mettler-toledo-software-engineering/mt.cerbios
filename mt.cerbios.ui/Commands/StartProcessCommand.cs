﻿using System;
using mt.cerbios.application.Services;
using mt.cerbios.ui.State;
using mt.cerbios.ui.ViewModels;
using mt.cerbios.ui.Views;
using mt.libraries.domain.Exceptions;
using MT.Singularity.Presentation.Controls.Navigation;
using MT.Singularity.Presentation.Model;

namespace mt.cerbios.ui.Commands
{
    public class StartProcessCommand : ICommand
    {
        private readonly IFormulationDataStore _formulationDataStore;
        private readonly IFormulationDataValidationService _dataValidation;
        private INavigationPage _processView;
        private HomeScreenViewModel _viewModel;
        public StartProcessCommand(HomeScreenViewModel viewModel, ProcessView processView, IFormulationDataStore formulationDataStore, IFormulationDataValidationService dataValidationService)
        {
            _viewModel = viewModel;
            _processView = processView;
            _formulationDataStore = formulationDataStore;
            _dataValidation = dataValidationService;
            
        }

        public event EventHandler CanExecuteChanged;

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            bool isValid = false;
            try
            {
                isValid = _dataValidation.FormulationDataIsValid(_formulationDataStore.CurrentFormulation);
            }
            catch (MissingDataException missingData)
            {
                _viewModel.ErrorMessage = $"{missingData.PropertyName} {Localization.Get(Localization.Key.EMNotEmpty)}";
            }
            catch (InvalidDateEnteredException invalidDate)
            {
                _viewModel.ErrorMessage = $"{invalidDate.PropertyName} {Localization.Get(Localization.Key.EMInvalidDate)}";
            }
            catch (InvalidCharacterException invalidCharacter)
            {
                _viewModel.ErrorMessage = $"{invalidCharacter.PropertyName} {Localization.Get(Localization.Key.EMInvalidCharacter)} {invalidCharacter.InvalidCharacter}";
            }
            catch (Exception e)
            {
                _viewModel.ErrorMessage = $"{Localization.Get(Localization.Key.EMStartProcessFailed)} {e}";
            }

            if (isValid)
            {
                _viewModel.ParentPage.NavigationService.NavigateTo(_processView);
            }

        }
        
    }
}
