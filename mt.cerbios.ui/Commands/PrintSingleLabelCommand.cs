﻿using System;
using System.Threading.Tasks;
using mt.cerbios.application.Models;
using mt.cerbios.application.Printing;
using mt.cerbios.ui.Infrastructure;
using mt.cerbios.ui.State;
using mt.cerbios.ui.ViewModels;
using mt.libraries.domain.Peripherals.Serial;
using MT.Singularity.Composition;
using MT.Singularity.Platform.Devices.Scale;

namespace mt.cerbios.ui.Commands
{

    public class PrintSingleLabelCommand : PrintLabelBaseCommand
    {

        

        public PrintSingleLabelCommand(IFormulationDataStore formulationData, ISerialLabelPrinter serialLabelPrinter, IScaleService scaleService) : base(formulationData, serialLabelPrinter, scaleService)
        {
        }


        protected override void AddNewFormulationItem(WeightInformation weight)
        {
            if (_formulationDataStore?.CurrentFormulation != null)
            {
                IFormulationItem formulationItem = new FormulationItem(_formulationDataStore?.CurrentFormulation?.Lot);
                formulationItem.CapturedTime = DateTime.Now;
                formulationItem.CapturedWeight = weight;
                formulationItem.ItemNumber = _formulationDataStore.CurrentFormulation.TotalNumberOfItems + 1;

                _formulationDataStore.CurrentFormulation.FormulationItems.Add(formulationItem);
                _formulationDataStore.RaiseStateChanged();
            }

        }

        protected override string GetPrinterTemplate(WeightInformation weight)
        {
            IPrintLabel printLabel = new PrintLabelSingle(Globals.NumberOfPrintLabels, _formulationDataStore.CurrentFormulation); ;
 
            string printtemplate = printLabel.GetPrintLabelTemplate();
            return printtemplate;
        }
    }
}
