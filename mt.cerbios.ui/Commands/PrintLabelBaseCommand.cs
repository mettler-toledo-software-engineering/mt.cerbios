﻿using System;

using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using mt.cerbios.application.Models;
using mt.cerbios.application.Printing;
using mt.cerbios.ui.Infrastructure;
using mt.cerbios.ui.State;
using mt.cerbios.ui.ViewModels;
using mt.libraries.domain.Peripherals.Serial;
using MT.Singularity.Platform.Devices.Scale;

namespace mt.cerbios.ui.Commands
{
    public abstract class PrintLabelBaseCommand : AsyncCommandBase
    {
        protected IFormulationDataStore _formulationDataStore;
        protected readonly ISerialLabelPrinter _serialLabelPrinter;
        protected readonly IScaleService _scaleService;



        public PrintLabelBaseCommand(IFormulationDataStore formulationData, ISerialLabelPrinter serialLabelPrinter, IScaleService scaleService)
        {
            _formulationDataStore = formulationData;
            _serialLabelPrinter = serialLabelPrinter;
            _scaleService = scaleService;
        }

        public PrintLabelBaseCommand(ISerialLabelPrinter serialLabelPrinter, IScaleService scaleService) 
        {
            _serialLabelPrinter = serialLabelPrinter;
            _scaleService = scaleService;
        }
        public override async Task ExecuteAsync(object parameter)
        {
            var processViewModel = parameter as ProcessViewModel;

            try
            {
                var weight = await _scaleService.SelectedScale.GetStableWeightAsync(UnitType.Display, Globals.GetWeightTimeOutInMs);

                if (weight == null)
                {
                    if (processViewModel != null)
                    {
                        processViewModel.ErrorMessage = $"Could not get stable net weight from scale.";
                    }
                }
                else if (weight.NetWeight < 0)
                {
                    if (processViewModel != null)
                    {
                        processViewModel.ErrorMessage = $"{Localization.Get(Localization.Key.EMNetSmallerZero)}";
                    }
                }
                else
                {
                    AddNewFormulationItem(weight);

                    string printTemplate = GetPrinterTemplate(weight);

                    await _serialLabelPrinter.Print(printTemplate);
                }

            }
            catch (Exception e)
            {
                if (processViewModel != null)
                {
                    processViewModel.ErrorMessage = $"Print Failed: {e.Message}";
                }
            }
        }

        protected virtual void AddNewFormulationItem(WeightInformation weight)
        {
        }

        protected abstract string GetPrinterTemplate(WeightInformation weight);

    }
}
