﻿using System;

using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using mt.cerbios.application.Models;
using mt.cerbios.application.Services;
using mt.cerbios.ui.State;
using mt.cerbios.ui.ViewModels;
using mt.libraries.domain.Exceptions;
using MT.Singularity.Presentation.Model;

namespace mt.cerbios.ui.Commands
{
    public class DeleteLastFormulationItemCommand : ICommand
    {
        public event EventHandler CanExecuteChanged;
        private readonly ProcessViewModel _processViewModel;
        private readonly IFormulationDataService _formulationDataService;
        private readonly IFormulationDataStore _formulationDataStore;

        public DeleteLastFormulationItemCommand(ProcessViewModel processViewModel, IFormulationDataService formulationDataService, IFormulationDataStore formulationDataStore)
        {
            _processViewModel = processViewModel;
            _formulationDataService = formulationDataService;
            _formulationDataStore = formulationDataStore;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            try
            {
                _formulationDataService.DeleteFormulationData.DeleteLastFormulationEntry(_formulationDataStore.CurrentFormulation);
                _processViewModel.FormulationItems = new ObservableCollection<IFormulationItem>(_formulationDataStore.CurrentFormulation.FormulationItems);
            }
            catch (EmptyListException)
            {
                _processViewModel.ErrorMessage = $"{Localization.Get(Localization.Key.EMNothingToDelete)}";
            }
            catch (Exception e)
            {
                _processViewModel.ErrorMessage = $"Deletion of Last Item Failed. {e.Message}";
            }
        }
    }
}
