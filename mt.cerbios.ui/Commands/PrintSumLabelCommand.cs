﻿using System;

using System.Collections.Generic;
using System.Text;
using mt.cerbios.application.Printing;
using mt.cerbios.ui.Infrastructure;
using mt.cerbios.ui.State;
using mt.cerbios.ui.ViewModels;
using mt.libraries.domain.Peripherals.Serial;
using MT.Singularity.Platform.Devices.Scale;

namespace mt.cerbios.ui.Commands
{
    public class PrintSumLabelCommand : PrintLabelBaseCommand
    {
        public PrintSumLabelCommand(IFormulationDataStore formulationData, ISerialLabelPrinter serialLabelPrinter, IScaleService scaleService) : base(formulationData, serialLabelPrinter, scaleService)
        {
        }

        protected override string GetPrinterTemplate(WeightInformation weight)
        {
            IPrintLabel printLabel =  new PrintLabelSum(Globals.NumberOfPrintLabels, _formulationDataStore.CurrentFormulation);


            string printtemplate = printLabel.GetPrintLabelTemplate();
            return printtemplate;
        }
    }
}
