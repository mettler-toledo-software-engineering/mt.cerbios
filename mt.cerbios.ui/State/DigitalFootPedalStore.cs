﻿using System;

using System.Collections.Generic;
using System.Text;
using mt.cerbios.application.Models;
using mt.cerbios.ui.Commands;
using mt.cerbios.ui.ViewModels;
using mt.libraries.domain.Peripherals.DigitalIo;
using mt.libraries.domain.Peripherals.Serial;
using MT.Singularity.Composition;
using MT.Singularity.Platform.Devices.Scale;

namespace mt.cerbios.ui.State
{
    [Export(typeof(IDigitalFootPedalStore))]
    [InjectionBehavior(IsSingleton = true)]
    public class DigitalFootPedalStore : IDigitalFootPedalStore
    {
        public event Action StateChanged;
        public DigitalFootPedalStore(IDigitalIoFootPedal digitalIoFootPedal, IScaleService scaleService, IFormulationDataStore formulationData, ISerialLabelPrinterStore serialLabelPrinter)
        {
            _digitalIoFootPedal = digitalIoFootPedal;
            UsedLabelPrinter = serialLabelPrinter.ConfiguredSerialLabelPrinter;
            _digitalIoFootPedal.LeftPedalIoChannel = 0;
            _digitalIoFootPedal.RightPedalIoChannel = 1;
            _digitalIoFootPedal.LeftPedalTask = new TareScaleCommand(scaleService);
            _digitalIoFootPedal.RightPedalTask = new PrintSingleLabelCommand(formulationData, UsedLabelPrinter, scaleService);
            ScaleService = scaleService;
        }

        private IDigitalIoFootPedal _digitalIoFootPedal;

        public IDigitalIoFootPedal ConfiguredFootPedal
        {
            get { return _digitalIoFootPedal; }
            set
            {
                _digitalIoFootPedal = value;
                StateChanged?.Invoke();
            }
        }

        private ISerialLabelPrinter _usedLabelPrinter;

        public ISerialLabelPrinter UsedLabelPrinter
        {
            get { return _usedLabelPrinter; }
            set
            {
                _usedLabelPrinter = value;
                StateChanged?.Invoke();
            }
        }

        public IScaleService ScaleService { get; }

    }
}
