﻿using System;
using mt.libraries.domain.Peripherals.Serial;
using MT.Singularity.Platform.Infrastructure;

namespace mt.cerbios.ui.State
{
    public interface ISerialLabelPrinterStore
    {
        event Action StateChanged;
        IPlatformEngine PlatformEngine { get; set; }
        ISerialInterfaceProviderService SerialInterfaceProviderService { get; set; }
        ISerialLabelPrinter ConfiguredSerialLabelPrinter { get; set; }
    }
}