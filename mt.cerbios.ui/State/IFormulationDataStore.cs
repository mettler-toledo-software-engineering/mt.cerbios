﻿using System;
using System.Collections.Generic;
using mt.cerbios.application.Models;

namespace mt.cerbios.ui.State
{
    public interface IFormulationDataStore
    {
        IFormulationData CurrentFormulation { get; set; }

        event Action StateChanged;
        void RaiseStateChanged();
    }
}