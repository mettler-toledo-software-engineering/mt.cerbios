﻿using System;

using System.Collections.Generic;
using System.Linq;
using System.Text;
using mt.cerbios.application.Models;
using MT.Singularity.Composition;

namespace mt.cerbios.ui.State
{
    [Export(typeof(IFormulationDataStore))]
    [InjectionBehavior(IsSingleton = true)]
    public class FormulationDataStore : IFormulationDataStore
    {
        public FormulationDataStore(IFormulationData formulationData)
        {
            CurrentFormulation = formulationData;

        }
        private IFormulationData _currentFormulation;

        public IFormulationData CurrentFormulation
        {
            get { return _currentFormulation; }
            set
            {
                if (_currentFormulation != value)
                {
                    _currentFormulation = value;
                    StateChanged?.Invoke();
                }
            }
        }


        public event Action StateChanged;

        public void RaiseStateChanged()
        {
            StateChanged?.Invoke();
        }

    }
}
