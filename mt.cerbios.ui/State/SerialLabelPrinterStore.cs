﻿using System;

using System.Collections.Generic;
using System.Text;
using mt.cerbios.ui.Config.State;
using mt.libraries.domain.Peripherals.Serial;
using mt.libraries.peripherals.SerialDevices;
using MT.Singularity.Composition;
using MT.Singularity.Platform.Infrastructure;

namespace mt.cerbios.ui.State
{
    [Export(typeof(ISerialLabelPrinterStore))]
    [InjectionBehavior(IsSingleton = true)]
    public class SerialLabelPrinterStore : ISerialLabelPrinterStore
    {
        public event Action StateChanged;
        public IPlatformEngine PlatformEngine { get; set; }
        public ISerialInterfaceProviderService SerialInterfaceProviderService { get; set; }
        private ISerialLabelPrinter _serialLabelPrinter;
        public SerialLabelPrinterStore(IPlatformEngine engine, ISerialInterfaceProviderService interfaceProvider, int physicalPort)
        {
            PlatformEngine = engine;
            SerialInterfaceProviderService = interfaceProvider;
            ConfiguredSerialLabelPrinter = new SerialLabelPrinter(engine, interfaceProvider, physicalPort);
        }

        public ISerialLabelPrinter ConfiguredSerialLabelPrinter
        {
            get {return _serialLabelPrinter; } 
            set
            {
                _serialLabelPrinter = value;
                StateChanged?.Invoke();
            }
        }
    }
}
