﻿using System;
using mt.libraries.domain.Peripherals.DigitalIo;
using mt.libraries.domain.Peripherals.Serial;
using MT.Singularity.Platform.Devices.Scale;

namespace mt.cerbios.ui.State
{
    public interface IDigitalFootPedalStore
    {
        IDigitalIoFootPedal ConfiguredFootPedal { get; set; }
        ISerialLabelPrinter UsedLabelPrinter { get; }
        IScaleService ScaleService { get; }

        event Action StateChanged;
    }
}