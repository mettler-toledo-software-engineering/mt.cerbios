﻿using mt.cerbios.ui.Infrastructure;
using MT.Singularity.Data;
using MT.Singularity.Platform.Devices.Scale;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Controls.Navigation;

namespace mt.cerbios.ui.ViewModels
{
    public abstract class ViewModelBase : ObservableObject
    {

        protected IScaleService _scaleService;


        public INavigationPage ParentPage { get; set; }

        public string ProgVersion
        {
            get
            {
#if DEBUG
                return Globals.ProjectNumber + "\nSimple Formulation\n" + Globals.ProgVersionStr(true);
#else
                    return Globals.ProjectNumber + "\nSimple Formulation\n" + Globals.ProgVersionStr(false);
#endif
            }
        }

        public Visual ParentVisual => (Visual)ParentPage;
        public ChildWindow ParentChildWindow { get; set; }

        protected ViewModelBase()
        {
        }

        protected ViewModelBase(IScaleService scaleService)
        {
            _scaleService = scaleService;
        }

        public virtual void RegisterEventsForViewModel()
        {
            if (_scaleService != null)
            {
                _scaleService.SelectedScale.NewChangedWeightInDisplayUnit += SelectedScaleOnNewChangedWeightInDisplayUnit;
            }
        }



        public virtual void UnregisterEventsForViewModel()
        {
            if (_scaleService != null)
            {
                _scaleService.SelectedScale.NewChangedWeightInDisplayUnit -= SelectedScaleOnNewChangedWeightInDisplayUnit;
            }

        }

        protected virtual void SelectedScaleOnNewChangedWeightInDisplayUnit(WeightInformation weight)
        {

        }


    }
}
