﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using mt.cerbios.application.Models;
using mt.cerbios.application.Services;
using mt.cerbios.ui.Commands;
using mt.cerbios.ui.Config.State;
using mt.cerbios.ui.State;
using mt.libraries.domain.Extensions;
using mt.libraries.domain.Peripherals.DigitalIo;
using mt.libraries.domain.Peripherals.Serial;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Model;

namespace mt.cerbios.ui.ViewModels
{
    public class ProcessViewModel : ViewModelBase
    {

        private readonly IFormulationDataStore _formulationData;
        private readonly IDigitalIoFootPedal _digitalIoFootPedal;
        private const int MaximumNumberOfDisplayableItems = 22;

        public ProcessViewModel(IFormulationDataStore formulationData, IDigitalFootPedalStore footPedalStore, ICurrentConfigurationStore currentConfiguration, IFormulationDataService formulationDataService)
        {

            _formulationData = formulationData;

            _digitalIoFootPedal = footPedalStore.ConfiguredFootPedal;
            BackCommand = new FinishProcessCommand(this, formulationData, currentConfiguration);
            PrintSingleCommand = new PrintSingleLabelCommand(_formulationData, footPedalStore.UsedLabelPrinter, footPedalStore.ScaleService);
            PrintSumCommand = new PrintSumLabelCommand(_formulationData, footPedalStore.UsedLabelPrinter, footPedalStore.ScaleService);
            RePrintCommand = new RePrintCommand(_formulationData, footPedalStore.UsedLabelPrinter, footPedalStore.ScaleService);
            DeleteLastFormulationItemCommand = new DeleteLastFormulationItemCommand(this, formulationDataService, formulationData);

            FormulationItems = new ObservableCollection<IFormulationItem>(_formulationData.CurrentFormulation.FormulationItems);
            
        }

        private void CanPrint(object sender, EventArgs e)
        {
            PrintEnabled = PrintSingleCommand.CanExecute(null) && PrintSumCommand.CanExecute(null) && RePrintCommand.CanExecute(null);
        }

        private void FormulationDataOnStateChanged()
        {
            FormulationItems = new ObservableCollection<IFormulationItem>(_formulationData.CurrentFormulation.FormulationItems.ToList().TakeLast(MaximumNumberOfDisplayableItems));

        }

        private ObservableCollection<IFormulationItem> _formulationItems;

        public ObservableCollection<IFormulationItem> FormulationItems
        {
            get { return _formulationItems; }
            set
            {
                _formulationItems = value;
                NotifyPropertyChanged(nameof(FormulationItems));
            }
        }

        public string Product => _formulationData.CurrentFormulation.Product;
        public string CodeNo => _formulationData.CurrentFormulation.Code;
        public string Lot => _formulationData.CurrentFormulation.Lot;
        public string RetestDate => _formulationData.CurrentFormulation.RetestDate.ToShortDateString();
        public string ExpiryDate => _formulationData.CurrentFormulation.ExpiryDate.ToShortDateString();

        private string _errorMessage;

        public string ErrorMessage
        {
            get { return _errorMessage; }
            set
            {

                if (string.IsNullOrWhiteSpace(value) == false)
                {
                    _errorMessage = value;
                    MessageBox.Show(ParentVisual, _errorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
        }



        private bool _printEnabled;

        public bool PrintEnabled
        {
            get { return _printEnabled; }
            set
            {
                if (_printEnabled != value)
                {
                    _printEnabled = value;
                    NotifyPropertyChanged(nameof(PrintEnabled));
                }
            }
        }


        public ICommand BackCommand { get; }
        public ICommand PrintSingleCommand { get; }
        public ICommand PrintSumCommand { get; }
        public ICommand RePrintCommand { get; }

        public ICommand DeleteLastFormulationItemCommand { get; }

        public void UpdateProperties()
        {
            NotifyPropertyChanged(nameof(Product));
            NotifyPropertyChanged(nameof(CodeNo));
            NotifyPropertyChanged(nameof(Lot));
            NotifyPropertyChanged(nameof(RetestDate));
            NotifyPropertyChanged(nameof(ExpiryDate));
            
            PrintEnabled = true;
        }

        public override void RegisterEventsForViewModel()
        {
            _digitalIoFootPedal.ActivateDevice = true;
            _formulationData.StateChanged += FormulationDataOnStateChanged;
            PrintSingleCommand.CanExecuteChanged += CanPrint;
            PrintSumCommand.CanExecuteChanged += CanPrint;
            RePrintCommand.CanExecuteChanged += CanPrint;

        }

        public override void UnregisterEventsForViewModel()
        {
            _digitalIoFootPedal.ActivateDevice = false;
            _formulationData.StateChanged -= FormulationDataOnStateChanged;
            PrintSingleCommand.CanExecuteChanged -= CanPrint;
            PrintSumCommand.CanExecuteChanged -= CanPrint;
            RePrintCommand.CanExecuteChanged -= CanPrint;
        }
    }
}
