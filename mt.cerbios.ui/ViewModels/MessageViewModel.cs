﻿using System;

using System.Collections.Generic;
using System.Text;

namespace mt.cerbios.ui.ViewModels
{
    public class MessageViewModel : ViewModelBase
    {
        private string _message;

        public string Message
        {
            get { return _message; }
            set
            {
                _message = value;
                NotifyPropertyChanged(nameof(Message));
                NotifyPropertyChanged(nameof(HasMessage));
            }
        }

        public bool HasMessage => string.IsNullOrWhiteSpace(Message) == false;

    }
}
