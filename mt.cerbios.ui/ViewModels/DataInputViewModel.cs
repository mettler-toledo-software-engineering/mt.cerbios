﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Threading.Tasks;
using mt.cerbios.ui.Config;
using mt.cerbios.ui.Config.State;
using mt.cerbios.ui.State;

namespace mt.cerbios.ui.ViewModels
{
    public class DataInputViewModel : ViewModelBase
    {
        private readonly IFormulationDataStore _formulationDataStore;
        private readonly ICurrentConfigurationStore _currentConfiguration;
        private int _retestDuration = 0;
        private int _expiryDuration = 0;
        

        public DataInputViewModel(IFormulationDataStore formulationDataStore, ICurrentConfigurationStore currentConfiguration)
        {
            _currentConfiguration = currentConfiguration;
            _formulationDataStore = formulationDataStore;
            _retestDuration = _currentConfiguration.CurrentConfiguration.RetestDuration;
            _expiryDuration = _currentConfiguration.CurrentConfiguration.ExpiryDuration;
            
            _currentConfiguration.CurrentConfiguration.PropertyChanged += CurrentConfigurationOnPropertyChanged;
            _formulationDataStore.StateChanged += FormulationDataStoreOnStateChanged;
            RetestDate = SetRetestDate();
        }

        private void FormulationDataStoreOnStateChanged()
        {
            RetestDate = SetRetestDate();
            ExpiryDate = SetExpiryDate();
            NotifyPropertyChanged(nameof(Product));
            NotifyPropertyChanged(nameof(CodeNo));
            NotifyPropertyChanged(nameof(Lot));
            NotifyPropertyChanged(nameof(RetestDate));

        }

        private string SetRetestDate()
        {
            var started = _formulationDataStore.CurrentFormulation.ProcessStarted;
            return started.AddDays(_retestDuration).ToShortDateString();
        }
        private string SetExpiryDate()
        {
            var started = _formulationDataStore.CurrentFormulation.ProcessStarted;
            return started.AddDays(_expiryDuration).ToShortDateString();
        }

        private void CurrentConfigurationOnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "RetestDuration")
            {
                _retestDuration = _currentConfiguration.CurrentConfiguration.RetestDuration;
                RetestDate = SetRetestDate();
            }
            if (e.PropertyName == "ExpiryDuration")
            {
                _expiryDuration = _currentConfiguration.CurrentConfiguration.ExpiryDuration;
                ExpiryDate = SetExpiryDate();
            }

        }

        public string Product
        {
            get { return _formulationDataStore.CurrentFormulation.Product; }
            set
            {
                _formulationDataStore.CurrentFormulation.Product = value;
                NotifyPropertyChanged(nameof(Product));
            }
        }

        public string CodeNo
        {
            get { return _formulationDataStore.CurrentFormulation.Code; }
            set
            {
                _formulationDataStore.CurrentFormulation.Code = value;
                NotifyPropertyChanged(nameof(CodeNo));
            }
        }

        public string Lot
        {
            get { return _formulationDataStore.CurrentFormulation.Lot; }
            set
            {
                _formulationDataStore.CurrentFormulation.Lot = value;
                NotifyPropertyChanged(nameof(Lot));
            }
        }

        public string RetestDate
        {
            get
            {
                return _formulationDataStore.CurrentFormulation.RetestDate.ToShortDateString(); ;
            }
            set
            {
                DateTime newDate;
                DateTime.TryParse(value, out newDate);
                _formulationDataStore.CurrentFormulation.RetestDate = newDate;
                NotifyPropertyChanged(nameof(RetestDate));
            }
        }

        public string ExpiryDate
        {
            get
            {
                return _formulationDataStore.CurrentFormulation.ExpiryDate.ToShortDateString();
            }

            set
            {
                DateTime newDate;
                DateTime.TryParse(value, out newDate);
                _formulationDataStore.CurrentFormulation.ExpiryDate = newDate;
                NotifyPropertyChanged(nameof(ExpiryDate));
            }
        }

    }
}
