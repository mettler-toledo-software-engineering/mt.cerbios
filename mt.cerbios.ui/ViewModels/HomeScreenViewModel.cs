﻿using mt.cerbios.application.Services;
using mt.cerbios.ui.Commands;
using mt.cerbios.ui.Config.State;
using mt.cerbios.ui.State;
using mt.cerbios.ui.Views;
using MT.Singularity.Platform.Devices.Scale;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Model;

namespace mt.cerbios.ui.ViewModels
{
    /// <summary>
    /// Home Screen view model implementation
    /// </summary>
    public class HomeScreenViewModel : ViewModelBase
    {

        
        /// <summary>
        /// Constructor Initializes a new instance of the <see cref="HomeScreenViewModel"/> class.
        /// </summary>

        public HomeScreenViewModel(IFormulationDataStore formulationDataStore, IFormulationDataValidationService dataValidationService, ICurrentConfigurationStore currentConfiguration, ProcessView processView, IScaleService scaleService, ISerialLabelPrinterStore serialLabelPrinterStore) : base(scaleService)
        {
            ResetDataFieldsCommand = new ResetDataFieldsCommand(formulationDataStore, currentConfiguration);
            StartProcessCommand = new StartProcessCommand(this, processView, formulationDataStore, dataValidationService);
            PrintTestLabelCommand = new PrintTestLabelCommand(serialLabelPrinterStore.ConfiguredSerialLabelPrinter, _scaleService);
        }

        private string _errorMessage;

        public string ErrorMessage
        {
            get { return _errorMessage; }
            set
            {
                if (string.IsNullOrWhiteSpace(value) == false)
                {
                    _errorMessage = value;
                    MessageBox.Show(ParentVisual, _errorMessage, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
        }
  

        public ICommand StartProcessCommand { get; }

        public ICommand ResetDataFieldsCommand { get; }

        public ICommand PrintTestLabelCommand { get; }

        public void UpdateProperties()
        {
            
        }
    }
}
