﻿using MT.Singularity.Presentation.Controls.Navigation;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Platform.CommonUX.Controls;
using MT.Singularity.Platform.CommonUX.Controls.MeasurementDisplay.View;
namespace mt.cerbios.ui.Views
{
    [System.CodeDom.Compiler.GeneratedCodeAttribute("YmlCodeGenerator", "2.1.0.0")]
    public partial class WeightWindowControl : MT.Singularity.Presentation.Controls.ContentControl
    {
        private MT.Singularity.Platform.CommonUX.Controls.MeasurementDisplay.View.WeightDisplayWindow myWeightWindow;
        private void InitializeComponents()
        {
            myWeightWindow = new MT.Singularity.Platform.CommonUX.Controls.MeasurementDisplay.View.WeightDisplayWindow();
            this.Content = myWeightWindow;
            this.Height = 300;
        }
    }
}
