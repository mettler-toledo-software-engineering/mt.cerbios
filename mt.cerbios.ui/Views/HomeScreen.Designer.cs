﻿using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Controls.Grid;
using MT.Singularity.Presentation.Controls.DataGrid;
using MT.Singularity.Presentation.Controls.Navigation;
using MT.Singularity.Platform.CommonUX.Controls;
using MT.Singularity.Platform.CommonUX.Controls.MeasurementDisplay.View;
using mt.cerbios.ui.Infrastructure;
namespace mt.cerbios.ui.Views
{
    [System.CodeDom.Compiler.GeneratedCodeAttribute("YmlCodeGenerator", "2.1.0.0")]
    public partial class HomeScreen : MT.Singularity.Presentation.Controls.Navigation.NavigationPage
    {
        private MT.Singularity.Presentation.Controls.DynamicDockPanel _dataInputControl;
        private MT.Singularity.Presentation.Controls.DynamicDockPanel _weightWindowControl;
        private void InitializeComponents()
        {
            MT.Singularity.Presentation.Controls.DockPanel internal1;
            MT.Singularity.Presentation.Controls.DockPanel internal2;
            MT.Singularity.Presentation.Controls.Grid.Grid internal3;
            MT.Singularity.Presentation.Controls.StackPanel internal4;
            MT.Singularity.Presentation.Controls.Image internal5;
            MT.Singularity.Presentation.Controls.TextBlock internal6;
            MT.Singularity.Presentation.Controls.DockPanel internal7;
            MT.Singularity.Platform.CommonUX.Controls.SoftKeyRibbon internal8;
            MT.Singularity.Platform.CommonUX.Controls.SoftKeyItem internal9;
            MT.Singularity.Platform.CommonUX.Controls.ZeroSoftkey internal10;
            MT.Singularity.Platform.CommonUX.Controls.SoftKeyItem internal11;
            MT.Singularity.Platform.CommonUX.Controls.TareSoftkey internal12;
            MT.Singularity.Platform.CommonUX.Controls.SoftKeyItem internal13;
            MT.Singularity.Platform.CommonUX.Controls.ClearSoftkey internal14;
            MT.Singularity.Platform.CommonUX.Controls.SoftKeyItem internal15;
            MT.Singularity.Platform.CommonUX.Controls.ImageButton internal16;
            MT.Singularity.Platform.CommonUX.Controls.SoftKeyItem internal17;
            MT.Singularity.Platform.CommonUX.Controls.ImageButton internal18;
            MT.Singularity.Platform.CommonUX.Controls.SoftKeyItem internal19;
            MT.Singularity.Platform.CommonUX.Controls.ImageButton internal20;
            _dataInputControl = new MT.Singularity.Presentation.Controls.DynamicDockPanel();
            _dataInputControl.Margin = new MT.Singularity.Presentation.Thickness(10);
            _dataInputControl.GridColumn = 0;
            _dataInputControl.GridRow = 0;
            _dataInputControl.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            _dataInputControl.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Left;
            _weightWindowControl = new MT.Singularity.Presentation.Controls.DynamicDockPanel();
            _weightWindowControl.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            _weightWindowControl.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Right;
            internal5 = new MT.Singularity.Presentation.Controls.Image();
            this.bindings[0] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal5.Source,() =>  Globals.CerbiosImage,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal5.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal6 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal6.Margin = new MT.Singularity.Presentation.Thickness(0, 10, 0, 0);
            this.bindings[1] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal6.Text,() =>  _homeScreenViewModel.ProgVersion,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal6.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal4 = new MT.Singularity.Presentation.Controls.StackPanel(_weightWindowControl, internal5, internal6);
            internal4.GridColumn = 1;
            internal4.GridRow = 0;
            internal4.Orientation = MT.Singularity.Presentation.Orientation.Vertical;
            internal3 = new MT.Singularity.Presentation.Controls.Grid.Grid(_dataInputControl, internal4);
            internal3.RowDefinitions = GridDefinitions.Create("1*");
            internal3.ColumnDefinitions = GridDefinitions.Create("1*", "1*");
            internal2 = new MT.Singularity.Presentation.Controls.DockPanel(internal3);
            internal2.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            internal9 = new MT.Singularity.Platform.CommonUX.Controls.SoftKeyItem();
            internal9.Index = 0;
            internal10 = new MT.Singularity.Platform.CommonUX.Controls.ZeroSoftkey();
            internal9.Content = internal10;
            internal11 = new MT.Singularity.Platform.CommonUX.Controls.SoftKeyItem();
            internal11.Index = 1;
            internal12 = new MT.Singularity.Platform.CommonUX.Controls.TareSoftkey();
            internal11.Content = internal12;
            internal13 = new MT.Singularity.Platform.CommonUX.Controls.SoftKeyItem();
            internal13.Index = 2;
            internal14 = new MT.Singularity.Platform.CommonUX.Controls.ClearSoftkey();
            internal13.Content = internal14;
            internal15 = new MT.Singularity.Platform.CommonUX.Controls.SoftKeyItem();
            internal15.Index = 3;
            internal16 = new MT.Singularity.Platform.CommonUX.Controls.ImageButton();
            internal16.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Bottom;
            internal16.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal16.Margin = new MT.Singularity.Presentation.Thickness(2, 0, 0, 2);
            internal16.Text = mt.cerbios.ui.Localization.Get(mt.cerbios.ui.Localization.Key.ResetForm);
            internal16.AddTranslationAction(() => {
                internal16.Text = mt.cerbios.ui.Localization.Get(mt.cerbios.ui.Localization.Key.ResetForm);
            });
            internal16.CustomClass = "ImageButtonDefaultHomeScreenButton";
            this.bindings[2] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal16.ImageSource,() =>  Globals.ClearImage,MT.Singularity.Expressions.BindingMode.OneWay,false);
            this.bindings[3] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal16.Command,() =>  _homeScreenViewModel.ResetDataFieldsCommand,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal15.Content = internal16;
            internal17 = new MT.Singularity.Platform.CommonUX.Controls.SoftKeyItem();
            internal17.Index = 4;
            internal18 = new MT.Singularity.Platform.CommonUX.Controls.ImageButton();
            internal18.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Bottom;
            internal18.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal18.Margin = new MT.Singularity.Presentation.Thickness(2, 0, 0, 2);
            internal18.Text = mt.cerbios.ui.Localization.Get(mt.cerbios.ui.Localization.Key.TestPrint);
            internal18.AddTranslationAction(() => {
                internal18.Text = mt.cerbios.ui.Localization.Get(mt.cerbios.ui.Localization.Key.TestPrint);
            });
            internal18.CustomClass = "ImageButtonDefaultHomeScreenButton";
            this.bindings[4] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal18.ImageSource,() =>  Globals.PrintImage,MT.Singularity.Expressions.BindingMode.OneWay,false);
            this.bindings[5] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal18.Command,() =>  _homeScreenViewModel.PrintTestLabelCommand,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal17.Content = internal18;
            internal19 = new MT.Singularity.Platform.CommonUX.Controls.SoftKeyItem();
            internal19.Index = 7;
            internal20 = new MT.Singularity.Platform.CommonUX.Controls.ImageButton();
            internal20.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Bottom;
            internal20.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal20.Margin = new MT.Singularity.Presentation.Thickness(2, 0, 0, 2);
            internal20.Text = mt.cerbios.ui.Localization.Get(mt.cerbios.ui.Localization.Key.Start);
            internal20.AddTranslationAction(() => {
                internal20.Text = mt.cerbios.ui.Localization.Get(mt.cerbios.ui.Localization.Key.Start);
            });
            internal20.CustomClass = "ImageButtonDefaultHomeScreenButton";
            this.bindings[6] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal20.ImageSource,() =>  Globals.StartImage,MT.Singularity.Expressions.BindingMode.OneWay,false);
            this.bindings[7] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal20.Command,() =>  _homeScreenViewModel.StartProcessCommand,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal19.Content = internal20;
            internal8 = new MT.Singularity.Platform.CommonUX.Controls.SoftKeyRibbon(internal9, internal11, internal13, internal15, internal17, internal19);
            internal8.SoftKeysPerPage = 8;
            internal8.Height = 90;
            internal7 = new MT.Singularity.Presentation.Controls.DockPanel(internal8);
            internal7.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Bottom;
            internal1 = new MT.Singularity.Presentation.Controls.DockPanel(internal2, internal7);
            this.Content = internal1;
        }
        private readonly MT.Singularity.Expressions.IDataBinding[] bindings = new MT.Singularity.Expressions.IDataBinding[8];
    }
}
