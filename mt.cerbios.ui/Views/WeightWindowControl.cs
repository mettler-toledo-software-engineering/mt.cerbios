﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Text;
using MT.Singularity;
using MT.Singularity.Composition;
using MT.Singularity.Presentation;
using MT.Singularity.Presentation.Controls;

namespace mt.cerbios.ui.Views
{
    /// <summary>
    /// Interaction logic for WeightWindowControl
    /// </summary>
    [Export(typeof(WeightWindowControl))]
    [InjectionBehavior(IsSingleton = true)]
    public partial class WeightWindowControl
    {
        public WeightWindowControl()
        {
            InitializeComponents();
            myWeightWindow.Activate();
        }
    }
}
