﻿using mt.cerbios.ui.ViewModels;
using MT.Singularity.Composition;
using MT.Singularity.Platform.CommonUX.Infrastructure;
using MT.Singularity.Presentation;
using MT.Singularity.Presentation.Controls.Navigation;

namespace mt.cerbios.ui.Views
{
    /// <summary>
    /// Interaction logic for HomeScreen
    /// </summary>
    [Export(typeof(IHomeScreenFactoryService))]
    public partial class HomeScreen : IHomeScreenFactoryService
    {
        private readonly HomeScreenViewModel _homeScreenViewModel;
        private readonly WeightWindowControl _weightWindow;
        private readonly DataInputView _dataInputView;


        /// <summary>
        /// Initializes a new instance of the <see cref="HomeScreen"/> class.
        /// </summary>
        public HomeScreen(HomeScreenViewModel homeScreenViewModel, WeightWindowControl weightWindow, DataInputView dataInputView)
        {
            _homeScreenViewModel = homeScreenViewModel;
            _weightWindow = weightWindow;
            _dataInputView = dataInputView;

            InitializeComponents();
        }

        /// <summary>
        /// Gets the home screen page.
        /// </summary>
        /// <value>
        /// The home screen page.
        /// </value>
        public INavigationPage HomeScreenPage
        {
            get { return this; }
        }

        /// <summary>
        /// This method is called before the home screen is shown.
        /// </summary>
        public void BeforeStart(IRootVisualProvider rootVisualProvider)
        {
        }



        /// <summary>
        /// Called when a page is being navigated to for the first time.
        /// </summary>
        protected override void OnFirstNavigation()
        {
            _weightWindowControl.Add(_weightWindow);
            _dataInputControl.Add(_dataInputView);
            _homeScreenViewModel.RegisterEventsForViewModel();
            _homeScreenViewModel.ParentPage = this;
            base.OnFirstNavigation();
        }

        /// <summary>
        /// Called when the user is navigating away from the current page to <paramref name="nextPage" />.
        /// </summary>
        /// <param name="nextPage">The next page the user is navigating to.</param>
        /// <returns>
        /// A value how the navigation framework should proceed with the navigation request.
        /// </returns>
        protected override NavigationResult OnNavigatingAway(INavigationPage nextPage)
        {
            var result = base.OnNavigatingAway(nextPage);
            _homeScreenViewModel.UnregisterEventsForViewModel();
            if (result == NavigationResult.Proceed)
            {
                _weightWindowControl.Remove(_weightWindow);
            }
            return result;
        }

        /// <summary>
        /// Called when a page is reactivated when returning from another page.
        /// </summary>
        /// <param name="previousPage">The page that the user is returning from.</param>
        protected override void OnNavigationReturning(INavigationPage previousPage)
        {
            base.OnNavigationReturning(previousPage);
            _homeScreenViewModel.RegisterEventsForViewModel();
            _homeScreenViewModel.UpdateProperties();
            _weightWindowControl.Add(_weightWindow);

        }

        protected override NavigationResult OnNavigatingBack(INavigationPage nextPage)
        {
            _homeScreenViewModel.UnregisterEventsForViewModel();
            return base.OnNavigatingBack(nextPage);
        }

        /// <summary>
        /// Gets a value indicating whether the cursor should be hidden.
        /// </summary>
        /// <value>
        ///   <c>true</c> to hide the cursor; otherwise, <c>false</c>.
        /// </value>
        public bool HideCursor => false;

    }
}
