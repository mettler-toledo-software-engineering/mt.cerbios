﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Text;
using mt.cerbios.ui.ViewModels;
using MT.Singularity;
using MT.Singularity.Presentation;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Controls.Navigation;

namespace mt.cerbios.ui.Views
{
    /// <summary>
    /// Interaction logic for ProcessView
    /// </summary>
    public partial class ProcessView
    {
        private ProcessViewModel _viewModel;
        private readonly WeightWindowControl _weightWindow;
        public ProcessView(ProcessViewModel processViewModel, WeightWindowControl weightWindow)
        {
            _viewModel = processViewModel;
            _weightWindow = weightWindow;
            InitializeComponents();
        }

        /// <summary>
        /// Gets the home screen page.
        /// </summary>
        /// <value>
        /// The home screen page.
        /// </value>
        public INavigationPage ProcessViewPage
        {
            get { return this; }
        }

        /// <summary>
        /// This method is called before the home screen is shown.
        /// </summary>
        public void BeforeStart(IRootVisualProvider rootVisualProvider)
        {
        }



        /// <summary>
        /// Called when a page is being navigated to for the first time.
        /// </summary>
        protected override void OnFirstNavigation()
        {
            _weightWindowControl.Add(_weightWindow);
            _viewModel.UpdateProperties();
            _viewModel.RegisterEventsForViewModel();
            _viewModel.ParentPage = ProcessViewPage;
            
            base.OnFirstNavigation();
        }

        /// <summary>
        /// Called when the user is navigating away from the current page to <paramref name="nextPage" />.
        /// </summary>
        /// <param name="nextPage">The next page the user is navigating to.</param>
        /// <returns>
        /// A value how the navigation framework should proceed with the navigation request.
        /// </returns>
        protected override NavigationResult OnNavigatingAway(INavigationPage nextPage)
        {
            var result = base.OnNavigatingAway(nextPage);
            _viewModel.UnregisterEventsForViewModel();
            if (result == NavigationResult.Proceed)
            {
                _weightWindowControl.Remove(_weightWindow);
            }
            return result;
        }

        /// <summary>
        /// Called when a page is reactivated when returning from another page.
        /// </summary>
        /// <param name="previousPage">The page that the user is returning from.</param>
        protected override void OnNavigationReturning(INavigationPage previousPage)
        {
            base.OnNavigationReturning(previousPage);
            _viewModel.RegisterEventsForViewModel();
            _viewModel.UpdateProperties();
            _viewModel.ParentPage = this;
            _weightWindowControl.Add(_weightWindow);

        }

        protected override NavigationResult OnNavigatingBack(INavigationPage nextPage)
        {
            _viewModel.UnregisterEventsForViewModel();
            _weightWindowControl.Remove(_weightWindow);
            return base.OnNavigatingBack(nextPage);
        }
    }
}
