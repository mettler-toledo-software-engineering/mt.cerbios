﻿using mt.cerbios.ui.ViewModels;
using MT.Singularity.Composition;

namespace mt.cerbios.ui.Views
{
    /// <summary>
    /// Interaction logic for DataInputView
    /// </summary>
    [Export(typeof(DataInputView))]
    [InjectionBehavior(IsSingleton = true)]
    public partial class DataInputView
    {
        private readonly DataInputViewModel _viewModel;
        public DataInputView(DataInputViewModel viewModel)
        {
            _viewModel = viewModel;
            InitializeComponents();
        }

    }
}
