﻿using MT.Singularity.Presentation.Controls.Navigation;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Platform.CommonUX.Controls;
using MT.Singularity.Platform.CommonUX.Controls.MeasurementDisplay.View;
using mt.cerbios.ui.Infrastructure;
namespace mt.cerbios.ui.Views
{
    [System.CodeDom.Compiler.GeneratedCodeAttribute("YmlCodeGenerator", "2.1.0.0")]
    public partial class DataInputView : MT.Singularity.Presentation.Controls.ContentControl
    {
        private void InitializeComponents()
        {
            MT.Singularity.Presentation.Controls.GroupPanel internal1;
            MT.Singularity.Presentation.Controls.StackPanel internal2;
            MT.Singularity.Presentation.Controls.TextBlock internal3;
            MT.Singularity.Presentation.Controls.TextBox internal4;
            MT.Singularity.Presentation.Controls.TextBlock internal5;
            MT.Singularity.Presentation.Controls.TextBox internal6;
            MT.Singularity.Presentation.Controls.TextBlock internal7;
            MT.Singularity.Presentation.Controls.TextBox internal8;
            MT.Singularity.Presentation.Controls.TextBlock internal9;
            MT.Singularity.Presentation.Controls.TextBox internal10;
            MT.Singularity.Presentation.Controls.TextBlock internal11;
            MT.Singularity.Presentation.Controls.TextBox internal12;
            internal3 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal3.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Left;
            internal3.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            internal3.Text = mt.cerbios.ui.Localization.Get(mt.cerbios.ui.Localization.Key.Product);
            internal3.AddTranslationAction(() => {
                internal3.Text = mt.cerbios.ui.Localization.Get(mt.cerbios.ui.Localization.Key.Product);
            });
            internal3.Foreground = Globals.White;
            internal3.Background = Globals.DarkBlue;
            internal3.Height = Globals.TextBlockHeight;
            internal3.Width = Globals.TextBlockWidth;
            internal3.Padding = Globals.DefaultPadding;
            internal3.FontSize = Globals.InputFontSize;
            internal3.Margin = Globals.BottomMargin10;
            internal4 = new MT.Singularity.Presentation.Controls.TextBox();
            this.bindings[0] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal4.Text,() =>  _viewModel.Product,MT.Singularity.Expressions.BindingMode.TwoWay,false);
            internal4.Height = Globals.TextBlockHeight;
            internal4.Width = Globals.TextBlockWidth;
            internal4.Padding = Globals.DefaultPadding;
            internal4.FontSize = Globals.InputFontSize;
            internal4.Margin = Globals.BottomMargin15;
            internal5 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal5.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Left;
            internal5.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            internal5.Text = mt.cerbios.ui.Localization.Get(mt.cerbios.ui.Localization.Key.CodeNo);
            internal5.AddTranslationAction(() => {
                internal5.Text = mt.cerbios.ui.Localization.Get(mt.cerbios.ui.Localization.Key.CodeNo);
            });
            internal5.Foreground = Globals.White;
            internal5.Background = Globals.DarkBlue;
            internal5.Height = Globals.TextBlockHeight;
            internal5.Width = Globals.TextBlockWidth;
            internal5.Padding = Globals.DefaultPadding;
            internal5.FontSize = Globals.InputFontSize;
            internal5.Margin = Globals.BottomMargin10;
            internal6 = new MT.Singularity.Presentation.Controls.TextBox();
            this.bindings[1] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal6.Text,() =>  _viewModel.CodeNo,MT.Singularity.Expressions.BindingMode.TwoWay,false);
            internal6.Height = Globals.TextBlockHeight;
            internal6.Width = Globals.TextBlockWidth;
            internal6.Padding = Globals.DefaultPadding;
            internal6.FontSize = Globals.InputFontSize;
            internal6.Margin = Globals.BottomMargin15;
            internal7 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal7.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Left;
            internal7.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            internal7.Text = mt.cerbios.ui.Localization.Get(mt.cerbios.ui.Localization.Key.Lot);
            internal7.AddTranslationAction(() => {
                internal7.Text = mt.cerbios.ui.Localization.Get(mt.cerbios.ui.Localization.Key.Lot);
            });
            internal7.Foreground = Globals.White;
            internal7.Background = Globals.DarkBlue;
            internal7.Height = Globals.TextBlockHeight;
            internal7.Width = Globals.TextBlockWidth;
            internal7.Padding = Globals.DefaultPadding;
            internal7.FontSize = Globals.InputFontSize;
            internal7.Margin = Globals.BottomMargin10;
            internal8 = new MT.Singularity.Presentation.Controls.TextBox();
            this.bindings[2] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal8.Text,() =>  _viewModel.Lot,MT.Singularity.Expressions.BindingMode.TwoWay,false);
            internal8.Height = Globals.TextBlockHeight;
            internal8.Width = Globals.TextBlockWidth;
            internal8.Padding = Globals.DefaultPadding;
            internal8.FontSize = Globals.InputFontSize;
            internal8.Margin = Globals.BottomMargin15;
            internal9 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal9.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Left;
            internal9.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            internal9.Text = mt.cerbios.ui.Localization.Get(mt.cerbios.ui.Localization.Key.RetestDate);
            internal9.AddTranslationAction(() => {
                internal9.Text = mt.cerbios.ui.Localization.Get(mt.cerbios.ui.Localization.Key.RetestDate);
            });
            internal9.Foreground = Globals.White;
            internal9.Background = Globals.DarkBlue;
            internal9.Height = Globals.TextBlockHeight;
            internal9.Width = Globals.TextBlockWidth;
            internal9.Padding = Globals.DefaultPadding;
            internal9.FontSize = Globals.InputFontSize;
            internal9.Margin = Globals.BottomMargin10;
            internal10 = new MT.Singularity.Presentation.Controls.TextBox();
            this.bindings[3] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal10.Text,() =>  _viewModel.RetestDate,MT.Singularity.Expressions.BindingMode.TwoWay,false);
            internal10.Height = Globals.TextBlockHeight;
            internal10.Width = Globals.TextBlockWidth;
            internal10.Padding = Globals.DefaultPadding;
            internal10.FontSize = Globals.InputFontSize;
            internal10.Margin = Globals.BottomMargin15;
            internal10.KeyboardLayout = MT.Singularity.Presentation.Input.KeyboardLayout.Numeric;
            internal11 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal11.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Left;
            internal11.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            internal11.Text = mt.cerbios.ui.Localization.Get(mt.cerbios.ui.Localization.Key.ExpiryDate);
            internal11.AddTranslationAction(() => {
                internal11.Text = mt.cerbios.ui.Localization.Get(mt.cerbios.ui.Localization.Key.ExpiryDate);
            });
            internal11.Foreground = Globals.White;
            internal11.Background = Globals.DarkBlue;
            internal11.Height = Globals.TextBlockHeight;
            internal11.Width = Globals.TextBlockWidth;
            internal11.Padding = Globals.DefaultPadding;
            internal11.FontSize = Globals.InputFontSize;
            internal11.Margin = Globals.BottomMargin10;
            internal12 = new MT.Singularity.Presentation.Controls.TextBox();
            this.bindings[4] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal12.Text,() =>  _viewModel.ExpiryDate,MT.Singularity.Expressions.BindingMode.TwoWay,false);
            internal12.Height = Globals.TextBlockHeight;
            internal12.Width = Globals.TextBlockWidth;
            internal12.Padding = Globals.DefaultPadding;
            internal12.FontSize = Globals.InputFontSize;
            internal12.Margin = Globals.BottomMargin15;
            internal12.KeyboardLayout = MT.Singularity.Presentation.Input.KeyboardLayout.Numeric;
            internal2 = new MT.Singularity.Presentation.Controls.StackPanel(internal3, internal4, internal5, internal6, internal7, internal8, internal9, internal10, internal11, internal12);
            internal2.Orientation = MT.Singularity.Presentation.Orientation.Vertical;
            internal1 = new MT.Singularity.Presentation.Controls.GroupPanel(internal2);
            this.Content = internal1;
        }
        private readonly MT.Singularity.Expressions.IDataBinding[] bindings = new MT.Singularity.Expressions.IDataBinding[5];
    }
}
