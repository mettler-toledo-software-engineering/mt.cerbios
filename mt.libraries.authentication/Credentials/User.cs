﻿using mt.libraries.domain.Authentication;
using MT.Singularity.Composition;

namespace mt.libraries.Authentication.Credentials
{
    [Export(typeof(IUser))]
    [InjectionBehavior(IsSingleton = true)]
    public class User : IUser
    {
        public string Username { get; set; }

        public string Password { get; set; }
    }
}
