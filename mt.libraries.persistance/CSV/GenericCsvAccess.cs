﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CsvHelper;
using CsvHelper.Configuration;
using log4net;
using mt.libraries.domain.Persistence.CSV;
using mt.libraries.domain.Persistence.Models;
using MT.Singularity.Logging;

namespace mt.libraries.persistance.CSV
{
    public class GenericCsvAccess<T> : IGenericCsvAccess<T> where T : IDomainObject
    {
        private static readonly ILog Logger = Log4NetManager.ApplicationLogger;
        private static readonly string SourceClass = nameof(GenericCsvAccess<T>);

        public string FileDelimiter { get; set; }
        public FileMode FileMode { get; set; }
        public bool HasHeaderRecord { get; set; }


        private readonly CsvConfiguration _config;

        

        public GenericCsvAccess(CsvConfiguration csvConfiguration)
        {

            _config = csvConfiguration;
            
            
        }

        public List<T> GetAllEntries(string filePath, string fileName)
        {
            try
            {
                var destination = Path.Combine(filePath, fileName);
                using (var reader = new StreamReader(destination))
                using (var csv = new CsvReader(reader, _config))
                {
                    var records = csv.GetRecords<T>().ToList();
                    return records;
                }
            }
            catch (Exception exception)
            {
                Logger.ErrorEx("Getting Entries failed : ",SourceClass,exception);
                return null;
            }
        }


        public bool SaveEntry(string filePath, string fileName, T dataEntry)
        {
            try
            {
                var destination = Path.Combine(filePath, fileName);
                using (var stream = File.Open(destination, FileMode.Append))
                using (var writer = new StreamWriter(stream))
                using (var csv = new CsvWriter(writer, _config))
                {
                    csv.WriteRecord<T>(dataEntry);
                    return true;
                }
            }
            catch (Exception e)
            {
                Logger.ErrorEx("Save entry failed : ", SourceClass, e);
                return false;
            }
        }


        public bool SaveEntries(string filePath, string fileName, List<T> dataEntries)
        {
            try
            {
                var destination = Path.Combine(filePath, fileName);
                using (var stream = File.Open(destination, FileMode.Append))
                using (var writer = new StreamWriter(stream))
                using (var csv = new CsvWriter(writer, _config))
                {
                    csv.WriteRecords(dataEntries);
                    return true;
                }
            }
            catch (Exception exception)
            {
                Logger.ErrorEx("Save Entries failed : ", SourceClass, exception);
                return false;
            }
        }


        public bool DeleteDataEntry(string filePath, string fileName, int id)
        {
            try
            {
                var destination = Path.Combine(filePath, fileName);
                List<T> records;
                using (var reader = new StreamReader(destination))
                using (var csv = new CsvReader(reader, _config))
                {
                    records = csv.GetRecords<T>().ToList();
                    records.RemoveAll(entry => entry.Id == id);
                }

                using (var stream = File.Open(destination, FileMode.Create))
                using (var writer = new StreamWriter(stream))
                using (var csv = new CsvWriter(writer, _config))
                {

                    csv.WriteRecords(records);

                }

                return true;

            }
            catch (Exception exception)
            {
                Logger.ErrorEx("Delete Entry failed : ", SourceClass, exception);
                return false;
            }

        }


        public int GenerateId(string filePath, string fileName)
        {
            List<T> records = new List<T>();

            try
            {
                var destination = Path.Combine(filePath, fileName);
                using (var reader = new StreamReader(destination))
                using (var csv = new CsvReader(reader, _config))
                {
                    records = csv.GetRecords<T>().ToList();

                }
            }
            catch (Exception e)
            {
                Logger.ErrorEx("Generate Id failed : ", SourceClass, e);
            }


            int? id = records.OrderByDescending(entry => entry.Id).FirstOrDefault()?.Id;

            if (id == null)
            {
                return 1;
            }
            else
            {
                return id.Value + 1;
            }
        }

    }
}
