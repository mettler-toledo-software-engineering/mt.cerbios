﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mt.libraries.domain.Persistence
{
    public interface IDataAccess<T>
    {
        List<T> GetAllDataEntries();
        bool SaveDataEntry(T dataEntry);

        bool SaveDataEntries(List<T> dataEntries);

        bool DeleteDataEntry(int id);

        int GenerateId();
    }
}
