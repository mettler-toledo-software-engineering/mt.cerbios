﻿using System.Collections.Generic;
using System.IO;
using mt.libraries.domain.Persistence.Models;

namespace mt.libraries.domain.Persistence.CSV
{
    public interface IGenericCsvAccess<T>  where T : IDomainObject
    {
        string FileDelimiter { get; set; }
        FileMode FileMode { get; set; }
        bool HasHeaderRecord { get; set; }

        List<T> GetAllEntries(string filePath, string fileName);
        bool SaveEntry(string filePath, string fileName, T dataEntry);
        bool SaveEntries(string filePath, string fileName, List<T> dataEntries);
        bool DeleteDataEntry(string filePath, string fileName, int id);
        int GenerateId(string filePath, string fileName);

    }
}
