﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mt.libraries.domain.Persistence.Models
{
    public interface IDomainObject
    {
        int Id { get; set; }
    }
}
