﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mt.libraries.domain.Exceptions
{
    public class MissingDataException : Exception
    {
        public string PropertyName { get; set; }

        public MissingDataException(string propertyName)
        {
            PropertyName = propertyName;
        }

        public MissingDataException(string propertyName, string message) : base(message)
        {
            PropertyName = propertyName;
        }
    }
}
