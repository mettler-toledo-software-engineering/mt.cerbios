﻿using System;

namespace mt.libraries.domain.Exceptions
{
    public class InvalidDateEnteredException : Exception
    {
        public string PropertyName { get; set; }

        public InvalidDateEnteredException(string propertyName)
        {
            PropertyName = propertyName;
        }

        public InvalidDateEnteredException(string propertyName, string message) : base(message)
        {
            PropertyName = propertyName;
        }
    }
}
