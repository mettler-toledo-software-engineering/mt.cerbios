﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MT.Singularity.Platform.Devices;

namespace mt.libraries.domain.Exceptions
{
    public class SerialInterfaceNotLoadedException : Exception
    {
        public ISerialInterface SerialInterface { get; set; }

        public SerialInterfaceNotLoadedException(ISerialInterface serialInterface)
        {
            SerialInterface = serialInterface;
        }
    }
}
