﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mt.libraries.domain.Exceptions
{
    public class InvalidCharacterException : Exception
    {
        public string PropertyName { get; set; }
        public string InvalidCharacter { get; set; }

        public InvalidCharacterException(string propertyName, string invalidCharacter)
        {
            PropertyName = propertyName;
            InvalidCharacter = invalidCharacter;
        }

        public InvalidCharacterException(string propertyName, string invalidCharacter, string message) : base(message)
        {
            PropertyName = propertyName;
            InvalidCharacter = invalidCharacter;
        }
    }
}
