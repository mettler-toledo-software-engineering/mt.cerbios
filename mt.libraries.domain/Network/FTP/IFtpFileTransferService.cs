﻿using System.Threading.Tasks;

namespace mt.libraries.domain.Network.FTP
{
    public interface IFtpFileTransferService
    {
        bool Updload(string filename, string sourceDirectory);
        Task<bool> UpdloadAsync(string filename, string sourceDirectory);
        bool Download(string filename, string targetDirectory);

    }
}