﻿using System.Threading.Tasks;
using MT.Singularity.Platform.Devices;
using MT.Singularity.Platform.Infrastructure;

namespace mt.libraries.domain.Peripherals.Serial
{
    public interface ISerialInterfaceProviderService
    {
        Task<ISerialInterface> FindSerialInterfacebyPhysicalPort(IPlatformEngine engine, int physicalPort);
        Task<ISerialInterface> FindSerialInterfaceByPortname(IPlatformEngine engine, string portname);
    }
}