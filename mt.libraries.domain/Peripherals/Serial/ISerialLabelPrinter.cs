﻿using System;
using System.Threading.Tasks;

namespace mt.libraries.domain.Peripherals.Serial
{
    public enum PrintResult
    {
        PrintOk,
        SerialChannelException

    }
    public interface ISerialLabelPrinter
    {
        event EventHandler<PrintResult> PrintExecutedEvent;
        Task Print(string template);
    }
}