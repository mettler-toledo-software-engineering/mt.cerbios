﻿namespace mt.libraries.domain.Peripherals.DigitalIo
{
    public interface IDigitalInputChangedEventArgs
    {
        int Input { get; }
        bool NewValue { get; }
    }
}