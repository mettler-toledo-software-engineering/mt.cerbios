﻿using System;


namespace mt.libraries.domain.Peripherals.DigitalIo
{
    public interface IDigitalIo
    {
        event EventHandler<IDigitalInputChangedEventArgs> DigitalInputChangedEvent;
    }
}