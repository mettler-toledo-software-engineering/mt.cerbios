﻿using System;
using System.Threading.Tasks;
using MT.Singularity.Presentation.Model;

namespace mt.libraries.domain.Peripherals.DigitalIo
{
    public enum ExecutionResult
    {
        NotExecuted,
        Executing,
        ExecutedOk,
        ExecutedWithError
    }
    public interface IDigitalIoFootPedal
    {

        int LeftPedalIoChannel { get; set; }
        ICommand LeftPedalTask { get; set; }
        int RightPedalIoChannel { get; set; }
        ICommand RightPedalTask { get; set; }

        bool ActivateDevice { get; set; }
        
        event EventHandler<ExecutionResult> PedelCommandExecuted;

    }
}