﻿namespace mt.libraries.domain.Authentication
{
    public interface IUser
    {
        string Username { get; set; }
        string Password { get; set; }
    }
}
